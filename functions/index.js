const functions = require('firebase-functions')
const admin = require('firebase-admin')

admin.initializeApp(functions.config().firebase)

exports.messageNotification = functions.firestore.document('conversations/{conversationUid}').onWrite((conversationSnap, context) => {
    const conversation = conversationSnap.after.data()
    if (conversation.messages.length === 0) return

    const lastMessage = conversation.messages[conversation.messages.length - 1].text
    const messageAuthor = conversation.messages[conversation.messages.length - 1].author

    const notificationContent = async () => {
        const authorSnap = await admin.firestore().collection('profiles').doc(messageAuthor).get()
        const authorName = authorSnap.data().name

        const notification = {
            title: authorName,
            body: lastMessage
        }

        const data = {
            type: 'message',
            conversationId: conversationSnap.after.id
        }

        let deviceUser

        if (conversation.members[0] !== messageAuthor) {
            deviceUser = conversation.members[0]
        }
        else if (conversation.members[1] !== messageAuthor) {
            deviceUser = conversation.members[1]
        }

        const receiverProfile = await admin.firestore().collection('profiles').doc(deviceUser).get()
        const receiverToken = receiverProfile.data().fcmToken

        admin.messaging().sendToDevice(receiverToken, { data, notification })
    }

    return notificationContent()
})
