import firebase from 'react-native-firebase'
import { getAllProfilesFromProfile } from '../helpers/profileHelper'

// Exported
export const getUserData = (userId) => {
    return new Promise((resolve, reject) => {
        const data = { profiles: [], activeProfile: {} }
        const promises = []

        promises.push(getUserProfiles(userId).then(profiles => data.profiles = profiles).catch(error => console.warn(error)))
        promises.push(getUserActiveProfile(userId).then(activeProfile => data.activeProfile = activeProfile).catch(error => console.warn(error)))

        Promise.all(promises).then(() => resolve(data)).catch(error => console.warn(error))
    })
}

export const switchProfile = (currentProfileId, nextProfileId) => {
    return new Promise((resolve, reject) => {
        if (currentProfileId === nextProfileId) return reject()

        const promise1 = firebase.firestore().collection('profiles').doc(currentProfileId).update({ active: false }).catch(err => { })
        const promise2 = firebase.firestore().collection('profiles').doc(nextProfileId).update({ active: true }).catch(err => { })

        Promise.all([promise1, promise2]).then(() => {
            firebase.firestore().collection('profiles').doc(nextProfileId).get().then(profileSnap => {
                const currentProfile = profileSnap.data()
                currentProfile.uid = profileSnap.id

                // Fetch avatar
                getAvatarUrl(currentProfile.avatar).then(url => {
                    currentProfile.avatar = url
                    resolve(currentProfile)
                }).catch(error => console.warn(error))
            }).catch(error => console.warn(error))
        }).catch(error => console.warn(error))
    })
}

export const createProfile = (profile, userId) => {
    return new Promise((resolve, reject) => {
        if (!userId) return reject('no userID')

        if (!profile.avatar.filename) {
            profile.avatar = `${profile.type}-avatar.png`

            firebase.firestore().collection('profiles').add(profile)
                .then(() => {
                    getUserProfiles(userId).then(profiles => {
                        resolve(profiles)
                    }).catch(error => console.warn(error))
                }).catch(error => console.warn(error))
        }
        else {
            firebase.storage().ref('avatars/' + profile.avatar.filename).putFile(decodeURI(profile.avatar.path))
                .then(() => {
                    profile.avatar = profile.avatar.filename
                    firebase.firestore().collection('profiles').add(profile)
                        .then(() => {
                            getUserProfiles(userId).then(profiles => resolve(profiles)).catch(error => console.warn(error))
                        }).catch(error => console.warn(error))
                }).catch(error => console.warn(error))
        }
    })
}

export const loginWithEmail = (email, password) => {
    return firebase.auth().signInWithEmailAndPassword(email, password)
}

export const resetPassword = (email) => {
    return firebase.auth().sendPasswordResetEmail(email)
}

export const register = ({ name, gender, birthday, email, password, passwordConf }) => {
    return new Promise((resolve, reject) => {
        // Check if passwords match
        if (password !== passwordConf) {
            return reject('passwords do not match')
        }

        // Create the new user
        firebase.auth().createUserWithEmailAndPassword(email, password)
            .then(response => {
                response.user.sendEmailVerification().then(() => {
                    response.user.updateEmail(email)

                    createHumanProfile(response.user.uid, name, gender, birthday).then(() => {
                        resolve()
                    }).catch(error => console.warn(error))
                }).catch(() => alert('something went wrong'))
            }).catch(error => console.warn(error))
    })
}

export const logout = (userId) => {
    return new Promise((resolve, reject) => {
        firebase.firestore().collection('profiles').where('user', '==', userId).get().then(profilesSnap => {
            profilesSnap.forEach(profile => {
                profile.ref.update({ fcmToken: '' })
            })

            firebase.auth().signOut().then(() => resolve()).catch(error => console.warn(error))
        }).catch(error => console.warn(error))
    })
}

export const getProfileById = (profileId) => {
    return new Promise((resolve, reject) => {
        firebase.firestore().collection('profiles').doc(profileId).get().then(profile => {
            const currentProfile = profile.data()
            currentProfile.uid = profile.id

            getAvatarUrl(currentProfile.avatar).then(url => {
                currentProfile.avatar = url
                resolve(currentProfile)
            }).catch(error => console.warn(error))
        }).catch(error => console.warn(error))
    })
}

export const getUserProfiles = (userId) => {
    return new Promise((resolve, reject) => {
        firebase.firestore().collection('profiles').where('user', '==', userId).get().then(profilesSnap => {
            const profilesArray = []
            const promises = []

            profilesSnap.forEach(profile => {
                const currentProfile = profile.data()
                currentProfile.uid = profile.id

                // Fetch avatar
                promises.push(getAvatarUrl(currentProfile.avatar).then(url => {
                    currentProfile.avatar = url
                    profilesArray.push(currentProfile)
                }).catch(error => console.warn(error)))
            })

            Promise.all(promises).then(() => resolve(profilesArray)).catch(error => console.warn(error))
        }).catch(error => console.warn(error))
    })
}

export const getUserNonHumanProfiles = (userId) => {
    return new Promise((resolve, reject) => {
        firebase.firestore().collection('profiles').where('user', '==', userId).get().then(profilesSnap => {
            const profilesArray = []
            const promises = []

            profilesSnap.forEach(profile => {
                const currentProfile = profile.data()

                if (currentProfile.type !== 'human') {
                    currentProfile.uid = profile.id

                    // Fetch avatar
                    promises.push(getAvatarUrl(currentProfile.avatar).then(url => {
                        currentProfile.avatar = url
                        profilesArray.push(currentProfile)
                    }).catch(error => console.warn(error)))
                }
            })

            Promise.all(promises).then(() => resolve(profilesArray)).catch(error => console.warn(error))
        }).catch(error => console.warn(error))
    })
}

export const getUserHumanProfile = (userId) => {
    return new Promise((resolve, reject) => {
        firebase.firestore().collection('profiles').where('user', '==', userId).get().then(profilesSnap => {
            profilesSnap.forEach(profile => {
                const currentProfile = profile.data()

                if (currentProfile.type === 'human') {
                    currentProfile.uid = profile.id

                    // Fetch avatar
                    getAvatarUrl(currentProfile.avatar).then(url => {
                        currentProfile.avatar = url
                        resolve(currentProfile)
                    }).catch(error => console.warn(error))
                }
            })
        }).catch(error => console.warn(error))
    })
}

const getUserActiveProfile = (userId) => {
    return new Promise((resolve, reject) => {
        firebase.firestore().collection('profiles').get().then(profilesSnap => {
            profilesSnap.forEach(profile => {
                if (profile.data().user === userId && profile.data().active) {
                    const currentProfile = profile.data()
                    currentProfile.uid = profile.id

                    // Fetch avatar
                    getAvatarUrl(currentProfile.avatar).then(url => {
                        currentProfile.avatar = url
                        resolve(currentProfile)
                    }).catch(error => console.warn(error))
                }
            })
        }).catch(error => console.warn(error))
    })
}

export const getAvatarUrl = (image) => {
    return new Promise((resolve, reject) => {
        firebase.storage().ref('avatars/' + image).getDownloadURL()
            .then(url => resolve(url)).catch(error => console.warn(error))
    })
}

export const getImageUrl = (image) => {
    return new Promise((resolve, reject) => {
        firebase.storage().ref('images/' + image).getDownloadURL()
            .then(url => resolve(url)).catch(error => console.warn(error))
    })
}

export const getImagesUrl = (images) => {
    return new Promise((resolve, reject) => {
        const urls = []

        images.forEach(image => {
            if (!image) {
                urls.push('')
                if (images.length === urls.length) {
                    resolve(urls)
                }
            }
            else {
                firebase.storage().ref('images/' + image).getDownloadURL().then(url => {
                    urls.push(url)
                    if (images.length === urls.length) {
                        resolve(urls)
                    }
                }).catch(error => console.warn(error))
            }
        })
    })
}

export const createHumanProfile = (userId, name, gender, birthday) => {
    return new Promise((resolve, reject) => {
        if (!userId) reject('no userID')

        const humanProfile = {
            user: userId,
            name: name,
            gender: gender,
            birthDate: birthday,
            type: 'human',
            avatar: 'human-avatar.png',
            active: true,
            geolocation: {
                city: '',
                country: '',
                countryCode: '',
                coords: {
                    lat: 0,
                    lng: 0
                }
            },
            swipedRight: [],
            swipedLeft: []
        }

        firebase.firestore().collection('profiles').add(humanProfile)
            .then(() => resolve(humanProfile)).catch(error => console.warn(error))
    })
}