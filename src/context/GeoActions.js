import { PermissionsAndroid } from 'react-native'
import Geocoder from 'react-native-geocoder'
import Geolocation from 'react-native-geolocation-service'

export const getGeolocation = () => {
    return new Promise((resolve, reject) => {
        Geolocation.getCurrentPosition(position => {
            Geocoder.geocodePosition({
                lat: position.coords.latitude,
                lng: position.coords.longitude
            }).then(response => resolve(response))
        },
            error => reject(error.message),
            { enableHighAccuracy: true, forceRequestLocation: true, timeout: 15000, maximumAge: 10000 }
        )
    })
}