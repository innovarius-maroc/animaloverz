export const initialState = { fetching: false, geolocation: null, error: '' }

export const GeoReducer = (state, action) => {
    switch (action.type) {
        case 'GEOLOCATION_FETCHING':
            return { ...state, fetching: true, geolocation: null, error: '' }
        case 'GEOLOCATION_SUCCESS':
            return { ...state, fetching: false, geolocation: action.geolocation, error: '' }
        case 'GEOLOCATION_FAILED':
            return { ...state, fetching: false, geolocation: null, error: action.error }
        default:
            return state
    }
}
