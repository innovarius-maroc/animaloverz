export const initialState = { loading: false, user: null, error: '' }

export const authReducer = (state, action) => {
    switch (action.type) {
        case 'REGISTER_ATTEMPT':
            return { ...state, loading: true, user: null, error: '' }
        case 'REGISTER_SUCCESS':
            return { ...state, loading: false, user: action.user, error: '' }
        case 'REGISTER_FAILED':
            return { ...state, loading: false, user: null, error: action.error }
        case 'LOGIN_ATTEMPT':
            return { ...state, loading: true, user: null, error: '' }
        case 'LOGIN_SUCCESS':
            return { ...state, loading: false, user: action.user, error: '' }
        case 'LOGIN_FAILED':
            return { ...state, loading: false, user: null, error: action.error }
        case 'LOGOUT_ATTEMPT':
            return { ...state, loading: true, user: null, error: '' }
        case 'LOGOUT_SUCCESS':
            return { ...state, loading: false, user: null, error: '' }
        case 'LOGOUT_FAILED':
            return { ...state, loading: false, user: null, error: action.error }
        case 'PROFILE_FETCHING':
            return { ...state, loading: true, error: '' }
        case 'PROFILE_SWITCHED':
            return { ...state, loading: false, user: action.user, error: '' }
        case 'PROFILE_CREATED':
            return { ...state, loading: false, user: action.user, error: '' }
        case 'PROFILE_UPDATED':
            return { ...state, loading: false, user: action.user, error: '' }
        case 'PROFILE_REMOVED':
            return { ...state, loading: false, user: action.user, error: '' }
        default:
            return state
    }
}
