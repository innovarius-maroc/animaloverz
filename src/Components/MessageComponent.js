import React from 'react'
import { View } from 'react-native'
import { Card, Text, Title } from 'react-native-paper'

const Message = () => {
    return (
        <View>
            <Card style={{ marginVertical: 10, marginHorizontal: 20, borderRadius: 5, elevation: 10, backgroundColor: '#EFD55C', width: '80%' }}>
                <View style={{ paddingHorizontal: 10 }}>
                    <View style={{ flexDirection: 'row', borderBottomColor: '#EBC92E', borderBottomWidth: 1 }}>
                        <Title style={{ fontSize: 14, marginHorizontal: 'auto' }} >mouad</Title>
                        <Title style={{ fontSize: 14, marginLeft: 'auto' }} >13:25</Title>
                    </View>

                    <Text style={{ fontSize: 16, marginVertical: 8 }}>Lorem Ipsum est simplement du faux texte employé dans la composition et la mise en page avant impression.</Text>
                </View>
            </Card>

            <Card style={{ marginVertical: 10, marginHorizontal: 20, borderRadius: 5, elevation: 10, backgroundColor: '#FEA494', width: '80%', marginLeft: 'auto' }}>
                <View style={{ paddingHorizontal: 10 }}>
                    <View style={{ flexDirection: 'row', borderBottomColor: '#FE8A76', borderBottomWidth: 1 }}>
                        <Title style={{ fontSize: 14, marginHorizontal: 'auto' }} >ayoub</Title>
                        <Title style={{ fontSize: 14, marginLeft: 'auto' }} >13:24</Title>
                    </View>

                    <Text style={{ fontSize: 16, marginVertical: 8 }}>I know what lorem ipsum means</Text>
                </View>
            </Card>

            <Card style={{ marginVertical: 10, marginHorizontal: 20, borderRadius: 5, elevation: 10, backgroundColor: '#EFD55C', width: '80%' }}>
                <View style={{ paddingHorizontal: 10 }}>
                    <View style={{ flexDirection: 'row', borderBottomColor: '#EBC92E', borderBottomWidth: 1 }}>
                        <Title style={{ fontSize: 14, marginHorizontal: 'auto' }} >mouad</Title>
                        <Title style={{ fontSize: 14, marginLeft: 'auto' }} >13:25</Title>
                    </View>

                    <Text style={{ fontSize: 16, marginVertical: 8 }}>Yeah I just tought I should remind you of what it means</Text>
                </View>
            </Card>

            <Card style={{ marginVertical: 10, marginHorizontal: 20, borderRadius: 5, elevation: 10, backgroundColor: '#FEA494', width: '80%', marginLeft: 'auto' }}>
                <View style={{ paddingHorizontal: 10 }}>
                    <View style={{ flexDirection: 'row', borderBottomColor: '#FE8A76', borderBottomWidth: 1 }}>
                        <Title style={{ fontSize: 14, marginHorizontal: 'auto' }} >ayoub</Title>
                        <Title style={{ fontSize: 14, marginLeft: 'auto' }} >13:24</Title>
                    </View>

                    <Text style={{ fontSize: 16, marginVertical: 8 }}>Wow okay thanks</Text>
                </View>
            </Card>
        </View>
    )
}

export default Message