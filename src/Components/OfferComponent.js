import React, { useState, useContext, useEffect } from 'react'
import { View, Image, Alert, TouchableWithoutFeedback, TouchableOpacity } from 'react-native'
import { Card, Text } from 'react-native-paper'
import { withNavigation } from 'react-navigation'
import { Menu, MenuOptions, MenuOption, MenuTrigger, renderers } from 'react-native-popup-menu'
import Icon from 'react-native-vector-icons/MaterialCommunityIcons'
import IconFontAwesome from 'react-native-vector-icons/FontAwesome'
import moment from 'moment'
import { createConversation, fetchConversation } from '../helpers/chatHelper'
import { firestore, storage } from 'react-native-firebase'
import { AuthContext } from '../../App'
import { setI18nConfig, translate } from '../i18n/i18n'
import * as RNLocalize from 'react-native-localize'
import Swipeable from 'react-native-swipeable'

const Offer = (props) => {

    const { auth, dispatch } = useContext(AuthContext)

    const [offer, setOffer] = useState(props.offer)
    const [isExpanded, setIsExpanded] = useState(false)

    // translation
    setI18nConfig()
    useEffect(() => {
        RNLocalize.addEventListener('change', setI18nConfig)
        return () => RNLocalize.removeEventListener('change', setI18nConfig)
    }, [])

    const handleCreateConversation = () => {
        if (auth.user.activeProfile.type !== 'human') {
            return alert(translate('useYourHumanProfile'))
        }

        createConversation([auth.user.activeProfile.uid, offer.author.uid]).then(() => {
            fetchConversation(auth.user.activeProfile.uid, offer.author.uid).then(conversation => {
                props.navigation.navigate('ConversationScreen', { conversation })
            })
        }).catch(error => alert(error))
    }

    const renderDescription = () => {
        const length = 28
        let text = offer.description

        if (text.length > length) {
            if (!isExpanded) {
                text = text.slice(0, length) + '..'
                return (
                    <Text>
                        {text}
                        <Text style={{ color: '#1024FF' }} onPress={() => setIsExpanded(true)}> {translate('showMore')}</Text>
                    </Text>
                )
            }
            else {
                return (
                    <Text>
                        {text}
                        <Text style={{ color: '#1024FF' }} onPress={() => setIsExpanded(false)}> {translate('showLess')}</Text>
                    </Text>
                )
            }
        }
        else {
            return <Text>{text}</Text>
        }
    }

    const renderGallery = () => {
        props.navigation.navigate('SwiperScreen', { images: offer.images })
    }

    const handleRemoveOffer = () => {
        Alert.alert(
            '',
            translate('confirm'),
            [
                { text: translate('no'), style: 'cancel' },
                {
                    text: translate('yes'), onPress: () => {
                        firestore().collection('offers').doc(offer.uid).delete().then(() => props.handleRefresh())
                        if (offer.type === 'product' || offer.type === 'service') {
                            for (let i = 0; i < 3; i++) {
                                storage().refFromURL(offer.images[i]).delete()
                            }
                        }
                    }
                }
            ]
        )
    }

    const renderRightButtons = () => {
        if (!auth || !auth.user || !auth.user.profiles) return
        let isFound = false

        auth.user.profiles.forEach(profile => {
            if (profile.uid === offer.author.uid) {
                isFound = true
            }
        })

        if (isFound) return [
            <TouchableWithoutFeedback onPress={handleRemoveOffer}>
                <View style={{ backgroundColor: '#E76650', height: '100%', justifyContent: 'center' }}>
                    <IconFontAwesome style={{ marginLeft: 20, color: '#FEA494' }} name='remove' size={40} />
                </View>
            </TouchableWithoutFeedback>
        ]

        else return [
            <TouchableWithoutFeedback onPress={() => props.navigation.navigate('ProfileScreen', { profile: offer.author })}>
                <View style={{ backgroundColor: '#D1EFEF', height: '100%', justifyContent: 'center' }}>
                    <Icon style={{ marginLeft: 16, color: '#05A8AA' }} name='account' size={40} />
                </View>
            </TouchableWithoutFeedback>,
            <TouchableWithoutFeedback onPress={handleCreateConversation}>
                <View style={{ backgroundColor: '#BAE7E7', height: '100%', justifyContent: 'center' }}>
                    <IconFontAwesome style={{ marginLeft: 18, color: '#05A8AA' }} name='commenting' size={34} />
                </View>
            </TouchableWithoutFeedback>
        ]
    }

    if (offer.type === 'animal') return (
        <Card style={{ marginVertical: 4, elevation: 2 }}>
            <Swipeable rightButtons={renderRightButtons()} rightButtonWidth={70}>
                <View style={{ flexDirection: 'row' }}>
                    <TouchableWithoutFeedback onPress={() => props.navigation.navigate('ProfileScreen', { profile: offer.animal })}>
                        <Image style={{ flex: 1, width: '100%', height: 100 }} source={{ uri: offer.animal.avatar }} />
                    </TouchableWithoutFeedback>

                    <View style={{ flex: 3, marginHorizontal: 5 }}>
                        <View style={{ flexDirection: 'column' }}>

                            <View style={{ flexDirection: 'row' }}>
                                <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                                    <Icon name='clock' size={14} color='#878A8C' />
                                    <Text style={{ fontSize: 14, color: '#878A8C', marginRight: 10, marginLeft: 3 }}>{moment(offer.createdAt).fromNow()}</Text>
                                </View>
                                <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                                    <Icon name='map-marker' size={16} color='#878A8C' />
                                    <Text style={{ fontSize: 14, color: '#878A8C', marginLeft: 1 }}>{offer.distance} km</Text>
                                </View>
                                <Icon name='chevron-double-left' size={26} color='#888' style={{ marginLeft: 'auto' }} />
                            </View>

                            <View style={{ marginTop: 10 }}>
                                <Text style={{ fontWeight: 'bold', color: '#05A8AA' }}>{offer.price}</Text>
                                {renderDescription()}
                            </View>
                        </View>
                    </View>
                </View>
            </Swipeable>
        </Card>
    )

    else if (offer.type === 'product' || offer.type === 'service') return (
        <Card style={{ marginVertical: 4, elevation: 2 }}>
            <Swipeable rightButtons={renderRightButtons()} rightButtonWidth={70}>
                <View style={{ flexDirection: 'row' }}>
                    <TouchableWithoutFeedback onPress={renderGallery}>
                        <Image style={{ flex: 1, width: '100%', height: 100 }} source={{ uri: offer.images[0] }} />
                    </TouchableWithoutFeedback>

                    <View style={{ flex: 3, marginHorizontal: 5 }}>
                        <View style={{ flexDirection: 'column' }}>

                            <View style={{ flexDirection: 'row' }}>
                                <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                                    <Icon name='clock' size={14} color='#878A8C' />
                                    <Text style={{ fontSize: 14, color: '#878A8C', marginRight: 10, marginLeft: 3 }}>{moment(offer.createdAt).fromNow()}</Text>
                                </View>
                                <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                                    <Icon name='map-marker' size={16} color='#878A8C' />
                                    <Text style={{ fontSize: 14, color: '#878A8C', marginLeft: 1 }}>{offer.distance} km</Text>
                                </View>
                                <Icon name='chevron-double-left' size={26} color='#888' style={{ marginLeft: 'auto' }} />
                            </View>

                            <View style={{ marginTop: 10 }}>
                                <Text style={{ fontWeight: 'bold', color: '#05A8AA' }}>{offer.price}</Text>
                                <Text>{offer.name}</Text>
                                {renderDescription()}
                            </View>
                        </View>
                    </View>
                </View>
            </Swipeable>
        </Card>
    )
}

export default withNavigation(Offer)
