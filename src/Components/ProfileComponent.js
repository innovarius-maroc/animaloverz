import React, { useState, useEffect, useContext } from 'react'
import { TouchableOpacity, ScrollView, View, ActivityIndicator, Alert, SafeAreaView } from 'react-native'
import { Avatar, Text } from 'react-native-paper'
import Modal from 'react-native-modal'
import { withNavigation } from 'react-navigation'
import { Menu, MenuOptions, MenuOption, MenuTrigger, renderers } from 'react-native-popup-menu'
import Icon from 'react-native-vector-icons/MaterialCommunityIcons'
import { AuthContext } from '../../App'
import { switchProfile, logout, getUserData } from '../context/AuthActions'
import { removeProfile } from '../helpers/profileHelper'
import { setI18nConfig, translate } from '../i18n/i18n'
import * as RNLocalize from 'react-native-localize'

const Profile = props => {

    const { auth, authDispatch } = useContext(AuthContext)

    const [profileModalVisible, setProfileModalVisible] = useState(false)
    const [avatar, setAvatar] = useState('')
    const [profiles, setProfiles] = useState([])

    // translation
    setI18nConfig()
    useEffect(() => {
        RNLocalize.addEventListener('change', setI18nConfig)
        return () => RNLocalize.removeEventListener('change', setI18nConfig)
    }, [])

    useEffect(() => {
        if (auth.user) {
            setAvatar(auth.user.activeProfile.avatar)
            setProfiles(auth.user.profiles)
        }
    }, [auth.user ? auth.user.activeProfile.avatar : null])

    useEffect(() => {
        if (auth.user) {
            setProfiles(auth.user.profiles)
        }
    }, [auth.user ? auth.user.profiles.length : null])

    const handleCreateNewProfile = () => {
        setProfileModalVisible(false)
        props.navigation.navigate('CreateProfileScreen')
    }

    const handleUpdateProfile = (profile) => {
        setProfileModalVisible(false)
        props.navigation.navigate('UpdateProfileScreen', { profile })
    }

    const handleSwitchProfile = (nextProfileId) => {
        const currentProfileId = auth.user.activeProfile.uid

        setProfileModalVisible(false)
        if (currentProfileId === nextProfileId) return

        authDispatch({ type: 'PROFILE_FETCHING' })

        switchProfile(currentProfileId, nextProfileId).then(profile => {
            auth.user.activeProfile = profile
            authDispatch({ type: 'PROFILE_SWITCHED', user: auth.user })
        })
    }

    const handleVisitProfile = (profile) => {
        setProfileModalVisible(false)
        props.navigation.navigate('ProfileScreen', { profile })
    }

    const handleRemoveProfile = (profileId, type) => {
        Alert.alert(
            '',
            translate('confirm'),
            [
                { text: 'NO', style: 'cancel' },
                {
                    text: 'YES', onPress: () => {
                        if (type === 'human') {
                            return alert(translate('youCannotRemovetheHumanProfile'))
                        }

                        setProfileModalVisible(false)
                        removeProfile(profileId).then(() => {
                            getUserData(auth.user.uid).then(user => {
                                authDispatch({ type: 'PROFILE_REMOVED', user: user })
                                alert(translate('removed'))
                            })
                        })
                    }
                }
            ]
        )
    }

    const handleLogout = () => {
        Alert.alert(
            '',
            translate('areYouSure'),
            [
                { text: 'NO', style: 'cancel' },
                {
                    text: 'YES', onPress: () => {
                        authDispatch({ type: 'LOGOUT_ATTEMPT' })
                        logout(auth.user.uid)
                            .then(() => authDispatch({ type: 'LOGOUT_SUCCESS' }))
                            .catch(() => authDispatch({ type: 'LOGOUT_FAILED' }))
                    }
                }
            ]
        )
    }

    const renderProfileAvatar = () => {
        if (auth.loading) return <ActivityIndicator style={{ marginLeft: 10 }} size={40} />

        else return (
            <Menu renderer={renderers.SlideInMenu}>
                <MenuTrigger>
                    <Avatar.Image size={45} style={{ marginLeft: 10, marginBottom: 10 }} source={{ uri: avatar }} />
                </MenuTrigger>
                <MenuOptions>
                    <MenuOption onSelect={() => handleVisitProfile(auth.user.activeProfile)} >
                        <View style={{ flexDirection: 'row', alignItems: 'center', paddingVertical: 8, paddingHorizontal: 10 }}>
                            <Icon style={{ marginRight: 15 }} name='account' size={25} />
                            <Text>{translate('visitProfile')}</Text>
                        </View>
                    </MenuOption>
                    <MenuOption onSelect={handleCreateNewProfile} >
                        <View style={{ flexDirection: 'row', alignItems: 'center', paddingVertical: 8, paddingHorizontal: 10 }}>
                            <Icon style={{ marginRight: 15 }} name='account-plus' size={25} />
                            <Text>{translate('createProfile')}</Text>
                        </View>
                    </MenuOption>
                    <MenuOption onSelect={() => handleUpdateProfile(auth.user.activeProfile)} >
                        <View style={{ flexDirection: 'row', alignItems: 'center', paddingVertical: 8, paddingHorizontal: 10 }}>
                            <Icon style={{ marginRight: 15 }} name='account-edit' size={25} />
                            <Text>{translate('updateProfile')}</Text>
                        </View>
                    </MenuOption>
                    <MenuOption onSelect={() => setProfileModalVisible(true)} >
                        <View style={{ flexDirection: 'row', alignItems: 'center', paddingVertical: 8, paddingHorizontal: 10 }}>
                            <Icon style={{ marginRight: 15 }} name='account-group' size={25} />
                            <Text>{translate('myProfiles')}</Text>
                        </View>
                    </MenuOption>
                    {/* <MenuOption onSelect={() => props.navigation.navigate('PreferencesScreen')} >
                        <View style={{ flexDirection: 'row', alignItems: 'center', paddingVertical: 8, paddingHorizontal: 10 }}>
                            <Icon style={{ marginRight: 15 }} name='settings' size={25} />
                            <Text style={{ alignSelf: 'center', marginVertical: 5 }}>{translate('preferences')}</Text>
                        </View>
                    </MenuOption> */}
                    <MenuOption onSelect={handleLogout} >
                        <View style={{ flexDirection: 'row', alignItems: 'center', paddingVertical: 8, paddingHorizontal: 10 }}>
                            <Icon style={{ marginRight: 15 }} name='logout-variant' size={25} />
                            <Text>{translate('logout')}</Text>
                        </View>
                    </MenuOption>
                </MenuOptions>
            </Menu>
        )
    }

    const renderProfiles = () => {
        if (profiles.length <= 1) {
            return <Text style={{ alignSelf: 'center' }}>{translate('yourProfilesWillAppearHere')}</Text>
        }

        return profiles.map((profile, key) => {
            if (auth.user && profile.uid === auth.user.activeProfile.uid) return null
            else return (
                <TouchableOpacity key={profile.uid} onPress={() => handleVisitProfile(profile)}>
                    <View style={{ flexDirection: 'row', alignItems: 'center', marginVertical: 5 }}>
                        <Avatar.Image size={60} style={{ marginLeft: 30, marginRight: 15 }} source={{ uri: profile.avatar }} />
                        <Text>{profile.name}</Text>
                        <View style={{ marginLeft: 'auto', flexDirection: 'row' }}>
                            <TouchableOpacity onPress={() => handleSwitchProfile(profile.uid)}>
                                <Icon style={{ marginRight: 20 }} name='account-check' size={40} />
                            </TouchableOpacity>
                            <TouchableOpacity onPress={() => handleRemoveProfile(profile.uid, profile.type)}>
                                <Icon style={{ marginRight: 10 }} name='account-remove' color='red' size={40} />
                            </TouchableOpacity>
                        </View>
                    </View>
                </TouchableOpacity>
            )
        })
    }

    return (
        <SafeAreaView>
            <Modal
                isVisible={profileModalVisible}
                useNativeDriver={true}
                onBackButtonPress={() => setProfileModalVisible(false)}
                onBackdropPress={() => setProfileModalVisible(false)}>
                <View style={{ justifyContent: 'center', alignItems: 'center', backgroundColor: '#FFF', padding: 10, borderRadius: 5 }}>
                    <ScrollView style={{ backgroundColor: 'transparent', width: '100%' }}>

                        {renderProfiles()}

                    </ScrollView>
                </View>
            </Modal>

            {renderProfileAvatar()}
        </SafeAreaView>
    )
}

export default withNavigation(Profile)
