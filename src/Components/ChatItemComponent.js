import React, { useState, useEffect } from 'react'
import { TouchableWithoutFeedback, View } from 'react-native'
import { Text, Card, Avatar } from 'react-native-paper'
import { withNavigation } from 'react-navigation'

const ChatItemComponent = props => {

    // const [lastMessage, setLastMessage] = useState(props.conversation.messages[props.conversation.messages.length - 1].text.slice(0, 80))
    // const [lastMessageTime, setLastMessageTime] = useState(new Date(props.conversation.messages[props.conversation.messages.length - 1].createdAt.seconds * 1000))

    return (
        <TouchableWithoutFeedback onPress={() => props.navigation.navigate('ConversationScreen', { conversation: props.conversation })}>
            <Card style={{ elevation: 2 }}>
                <View style={{ flexDirection: 'row', padding: 5, alignItems: 'center' }}>
                    <Avatar.Image style={{ marginRight: 20, marginLeft: 5 }} size={50} source={{ uri: props.conversation.member.avatar }} />
                    <Text style={{ fontSize: 18 }}>{props.conversation.member.name}</Text>

                    {/* 
                    <View style={{ flexDirection: 'row' }}>
                        <View style={{ flexDirection: 'column' }}>
                            <Text style={{ fontSize: 16 }}>{props.conversation.member.name}</Text>
                            <Text style={{ fontSize: 14, color: '#888' }}>{lastMessage}</Text>
                        </View>
                    </View>
                    */}

                    {/* <Text style={{ fontSize: 14, color: '#888', marginLeft: 'auto' }}>{moment(lastMessageTime).fromNow()}</Text> */}
                </View>
            </Card>
        </TouchableWithoutFeedback>
    )
}

export default withNavigation(ChatItemComponent)
