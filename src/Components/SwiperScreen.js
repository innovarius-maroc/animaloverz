import React, { useState } from 'react'
import { StyleSheet, Image, View } from 'react-native'
import Swiper from 'react-native-swiper'

const SwiperScreen = props => {

    const [images, setImages] = useState(props.navigation.state.params.images)
    const [index, setIndex] = useState(props.navigation.state.params.index)

    return (
        <Swiper
        style={styles.wrapper}
        showsButtons
        loadMinimal
        loop={false}
        index={index} >
            {
                images.map((image, index) => (
                    <View key={index} style={styles.slide}>
                        <Image source={{ uri: image }} style={{ width: '100%', height: '100%', resizeMode: 'contain' }} />
                    </View>
                ))
            }
        </Swiper>
    )
}

const styles = StyleSheet.create({
    wrapper: {},
    slide: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#444'
    }
})

export default SwiperScreen
