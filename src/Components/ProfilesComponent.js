import React, { useState, useEffect, useContext } from 'react'
import { TouchableOpacity, View, FlatList, Image } from 'react-native'
import { Avatar } from 'react-native-paper'
import { AuthContext } from '../../App'
import { switchProfile } from '../context/AuthActions'
import { withNavigation } from 'react-navigation'

const Profiles = props => {

    const { auth, authDispatch } = useContext(AuthContext)

    const [profiles, setProfiles] = useState([])

    useEffect(() => {
        setProfiles(auth.user.profiles)
    }, [])

    const handleSwitchProfile = (nextProfileId) => {
        const currentProfileId = auth.user.activeProfile.uid

        if (currentProfileId === nextProfileId) return

        authDispatch({ type: 'PROFILE_FETCHING' })

        switchProfile(currentProfileId, nextProfileId).then(profile => {
            auth.user.activeProfile = profile
            authDispatch({ type: 'PROFILE_SWITCHED', user: auth.user })
        })
    }

    return (
        <View style={{ backgroundColor: 'white' }}>
            <FlatList
                horizontal
                ListHeaderComponent={() => (
                    <TouchableOpacity onPress={() => props.navigation.navigate('CreateProfileScreen')}>
                        <Image style={{ width: 50, height: 50, margin: 5 }} source={require('../assets/images/plus.png')} />
                    </TouchableOpacity>
                )}
                data={profiles}
                renderItem={({ item }) => {
                    if (auth.user.activeProfile.uid !== item.uid) {
                        return (
                            (
                                <TouchableOpacity onPress={() => handleSwitchProfile(item.uid)}>
                                    <Image style={{ width: 50, height: 50, margin: 5, borderRadius: 25 }} source={{ uri: item.avatar }} />
                                </TouchableOpacity>
                            )
                        )
                    }
                }}
                keyExtractor={item => item.uid}
            />
        </View>
    )
}

export default withNavigation(Profiles)
