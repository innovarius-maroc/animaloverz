import React, { useState, useEffect, useContext } from 'react'
import { View, TouchableOpacity, Linking, Alert, TouchableWithoutFeedback } from 'react-native'
import { Avatar, Card, Text } from 'react-native-paper'
import { withNavigation } from 'react-navigation'
import { Menu, MenuOptions, MenuOption, MenuTrigger, renderers } from 'react-native-popup-menu'
import Icon from 'react-native-vector-icons/MaterialCommunityIcons'
import moment from 'moment'
import { firestore, storage } from 'react-native-firebase'
import { AuthContext } from '../../App'
import { setI18nConfig, translate } from '../i18n/i18n'
import * as RNLocalize from 'react-native-localize'

let canLike = true // Lock the button until the previous like is synced to database

const Article = (props) => {

    const { auth, dispatch } = useContext(AuthContext)

    const [article, setArticle] = useState(props.article)
    const [image, setImage] = useState(article.image)
    const [likes, setLikes] = useState(article.likes)
    const [isLiked, setIsLiked] = useState(false)
    const [shares, setShares] = useState(article.shares)
    const [isExpanded, setIsExpanded] = useState(false)

    // translation
    setI18nConfig()
    useEffect(() => {
        RNLocalize.addEventListener('change', setI18nConfig)
        return () => RNLocalize.removeEventListener('change', setI18nConfig)
    }, [])

    // Listen to likes and shares count
    useEffect(() => {
        const unsubscribe = firestore().collection('articles').doc(article.uid).onSnapshot(changedArticle => {
            if (changedArticle.exists) {
                setLikes(changedArticle.data().likes)
                setShares(changedArticle.data().shares)
            }
        })

        return unsubscribe
    }, [])

    // Listen to if active profiles likes current article
    useEffect(() => {
        const unsubscribe = firestore().collection('articleLikes').where('article', '==', article.uid).where('profile', '==', auth.user.activeProfile.uid).onSnapshot(articleLike => {
            setIsLiked(!articleLike.empty)
        })

        return unsubscribe
    }, [auth.user.activeProfile])

    // fetch article image
    useEffect(() => {
        if (article.type === 'image') {
            storage().ref('images/' + image).getDownloadURL().then(url => {
                setImage(url)
            })
        }
    }, [image])

    const likeArticle = () => {
        if (!canLike) return
        canLike = false

        firestore().collection('articleLikes').where('profile', '==', auth.user.activeProfile.uid).where('article', '==', article.uid).get()
            .then(articleLikesSnap => {
                // If not already liked
                if (articleLikesSnap.empty) {
                    firestore().collection('articleLikes').add({ profile: auth.user.activeProfile.uid, article: article.uid })
                        .then(() => {
                            const newLikes = likes + 1
                            setLikes(newLikes)
                            firestore().collection('articles').doc(article.uid).update({ likes: newLikes })
                                .then(() => canLike = true)
                        })
                }
                // If already liked
                else {
                    firestore().collection('articleLikes').doc(articleLikesSnap.docs[0].id).delete()
                        .then(() => {
                            const newLikes = likes - 1
                            setLikes(newLikes)
                            firestore().collection('articles').doc(article.uid).update({ likes: newLikes })
                                .then(() => canLike = true)
                        })
                }
            })
    }

    // const shareArticle = () => {
    //     const newShares = shares + 1
    //     setShares(newShares)
    //     firestore().collection('articles').doc(article.uid).update({ shares: newShares })
    // }

    const renderContent = () => {
        if (article.image) {
            return (
                <TouchableWithoutFeedback onPress={() => props.navigation.navigate('SwiperScreen', { images: [image], index: 0 })}>
                    <Card.Cover style={{ marginHorizontal: 10, height: 300, borderRadius: 5 }} source={{ uri: image }} />
                </TouchableWithoutFeedback>
            )
        }
        else if (article.content) {
            let text = article.content

            if (text.length > 200) {
                if (!isExpanded) {
                    text = text.slice(0, 200) + '..'
                    return (
                        <Text style={{ marginHorizontal: 10 }}>
                            {text}
                            <Text style={{ color: '#1024FF' }} onPress={() => setIsExpanded(true)}> show more</Text>
                        </Text>
                    )
                }
                else {
                    return (
                        <Text style={{ marginHorizontal: 10 }}>
                            {text}
                            <Text style={{ color: '#1024FF' }} onPress={() => setIsExpanded(false)}> show less</Text>
                        </Text>
                    )
                }
            }
            else {
                return <Text style={{ marginHorizontal: 10 }}>{text}</Text>
            }
        }
        else if (article.link) {
            return (
                <Card style={{ marginVertical: 10, marginHorizontal: 10, elevation: 2 }}>
                    <TouchableOpacity onPress={() => Linking.openURL(article.link.url)}>
                        <Card.Cover style={{ marginHorizontal: 10, marginTop: 10, height: 300 }} source={{ uri: article.link.preview }} />
                        <Text style={{ marginHorizontal: 10, marginVertical: 6, fontSize: 16 }}>{article.link.title}</Text>
                    </TouchableOpacity>
                </Card>
            )
        }
        else {
            return null
        }
    }

    const renderLikeButton = () => {
        if (isLiked) return (<Icon name="heart" color={'#DF0069'} size={28} />)
        else if (!isLiked) return (<Icon name="heart" color={'#888888'} size={28} />)
    }

    const renderOwnerControls = () => {
        if (article.author.uid === auth.user.activeProfile.uid) return (
            <Menu style={{ marginLeft: 'auto' }} renderer={renderers.SlideInMenu}>
                <MenuTrigger>
                    <Icon name='dots-vertical' size={25} />
                </MenuTrigger>
                <MenuOptions>
                    <MenuOption onSelect={handleRemoveArticle}>
                        <View style={{ flexDirection: 'row', alignItems: 'center', paddingVertical: 8, paddingHorizontal: 10 }}>
                            <Icon style={{ marginRight: 15 }} name='delete-forever' size={25} />
                            <Text>{translate('remove')}</Text>
                        </View>
                    </MenuOption>
                </MenuOptions>
            </Menu>
        )
    }

    const handleRemoveArticle = () => {
        Alert.alert(
            '',
            translate('areYouSure'),
            [
                { text: 'NO', style: 'cancel' },
                {
                    text: 'YES', onPress: () => {
                        if (article.type === 'text' || article.type === 'link') {
                            firestore().collection('articles').doc(article.uid).delete().then(() => props.handleRefresh())
                        } else if (article.type === 'image') {
                            storage().refFromURL(image).delete().then(() => {
                                firestore().collection('articles').doc(article.uid).delete().then(() => props.handleRefresh())
                            })
                        }
                    }
                }
            ]
        )
    }

    return (
        <Card style={{ marginVertical: 4, elevation: 2 }}>

            <View style={{ margin: 10, flexDirection: 'row', marginBottom: 5, paddingBottom: 5, borderBottomColor: '#ddd', borderBottomWidth: 1 }}>
                <TouchableOpacity onPress={() => props.navigation.navigate('ProfileScreen', { profile: article.author })}>
                    <Avatar.Image style={{ marginRight: 10 }} size={40} source={{ uri: article.author.avatar }} />
                </TouchableOpacity>

                <View style={{ flexDirection: 'column' }}>
                    <Text style={{ fontSize: 16 }} >{article.author.name}</Text>

                    <View style={{ flexDirection: 'row' }}>
                        <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                            <Icon name='clock' size={14} color='#878A8C' />
                            <Text style={{ fontSize: 14, color: '#878A8C', marginLeft: 3 }}>{moment(article.createdAt).fromNow()}</Text>
                        </View>
                        <View style={{ flexDirection: 'row', alignItems: 'center', marginLeft: 16 }}>
                            <Icon name='map-marker' size={14} color='#878A8C' />
                            <Text style={{ fontSize: 14, color: '#878A8C', marginLeft: 1 }} >{article.distance} {translate('km')}</Text>
                        </View>
                    </View>
                </View>


                {renderOwnerControls()}
            </View>

            <View style={{ marginBottom: 6 }}>
                <Text style={{ marginHorizontal: 10, marginBottom: 5, fontSize: 16 }}>{article.title}</Text>
                {renderContent()}
            </View>

            <View style={{ flexDirection: 'row', marginHorizontal: 10, marginTop: 10 }}>
                <Text style={{ paddingRight: 10, flexDirection: 'row', alignItems: 'baseline' }}>
                    <Icon name="heart" color={'#DF0069'} size={16} />
                    {' ' + likes}
                </Text>
                {/* <Text style={{ paddingRight: 10, flexDirection: 'row', alignItems: 'baseline' }}>
                    <Icon name="share" color={'#009FF2'} size={16} />
                    {' ' + shares}
                </Text> */}
            </View>

            <Card.Actions style={{ flexDirection: 'row', justifyContent: 'flex-end' }}>
                <TouchableOpacity style={{ position: 'absolute', top: -30, right: 5, elevation: 10, marginHorizontal: 10, borderRadius: 50, width: 40, height: 40, backgroundColor: '#fff', alignItems: 'center', justifyContent: 'center' }} onPress={likeArticle}>
                    {renderLikeButton()}
                </TouchableOpacity>

                {/* <TouchableOpacity style={{ position: 'absolute', top: -30, right: 10, elevation: 10, marginHorizontal: 10, borderRadius: 50, width: 40, height: 40, backgroundColor: '#fff', alignItems: 'center', justifyContent: 'center' }} onPress={shareArticle}>
                    <Icon name="share" color={'#009FF2'} size={28} />
                </TouchableOpacity> */}
            </Card.Actions>

        </Card>
    )
}

export default withNavigation(Article)