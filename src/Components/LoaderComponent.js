import React from 'react'
import { View, Image } from 'react-native'

export default Loader = props => {
    return (
        <View style={{ backgroundColor: '#EAEAEA', height: '100%', width: '100%' }}>
            <View style={{ justifyContent: 'center', alignItems: 'center', backgroundColor: 'transparent', width: '100%', height: '100%' }}>
                <Image style={{ width: 200, height: 200 }} source={require('../assets/images/paw.gif')} />
            </View>
        </View>
    )
}
