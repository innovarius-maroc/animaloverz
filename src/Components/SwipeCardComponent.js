import React, { useState, useEffect } from 'react'
import { View, Image, StyleSheet } from 'react-native'
import { Text } from 'react-native-paper'
import { setI18nConfig, translate } from '../i18n/i18n'
import * as RNLocalize from 'react-native-localize'

const SwipeCard = ({ profile }) => {

    // translation
    setI18nConfig()
    useEffect(() => {
        RNLocalize.addEventListener('change', setI18nConfig)
        return () => RNLocalize.removeEventListener('change', setI18nConfig)
    }, [])

    if (!profile) return (
        null
    )

    else return (
        <View>
            <View style={styles.card}>
                <Image source={{ uri: profile.avatar }} style={{ height: '100%', width: '100%' }} />
                <View style={{ backgroundColor: '#00000066', position: 'absolute', width: '100%', bottom: 0 }}>
                    <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'center' }}>
                        <Text style={{ color: '#FFF', fontSize: 36, marginLeft: 15, paddingBottom: 5, flex: 5 }}>{profile.name}</Text>
                        <Text style={{ color: '#FFF', fontSize: 25, marginLeft: 'auto', flex: 1 }}>{profile.distance} {translate('km')}</Text>
                    </View>
                </View>
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    card: {
        borderRadius: 4,
        borderWidth: 2,
        borderColor: "#E8E8E8",
        justifyContent: "center",
        backgroundColor: "white",
        height: '85%'
    }
})

export default SwipeCard