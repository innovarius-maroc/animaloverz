import React from 'react'

import { createAppContainer } from 'react-navigation'
import { createStackNavigator } from 'react-navigation-stack'

import HomeNavigator from './HomeNavigator'
import ProfileScreen from '../screens/App/Profil/ProfileScreen'
import CreateProfileScreen from '../screens/App/Profil/CreateProfileScreen'
import UpdateProfileScreen from '../screens/App/Profil/UpdateProfileScreen'
import ConversationScreen from '../screens/App/Chat/ConversationScreen'
import Profile from '../Components/ProfileComponent'
import SellAnimalScreen from '../screens/App/Market/SellAnimalScreen'
import SellProductScreen from '../screens/App/Market/SellProductScreen'
import SellServiceScreen from '../screens/App/Market/SellServiceScreen'
import SwiperScreen from '../Components/SwiperScreen'
import PreferencesScreen from '../screens/App/settings/PreferencesScreen'

const AppNavigator = createStackNavigator({
    HomeNavigator: {
        screen: HomeNavigator,
        navigationOptions: {
            headerLeft: <Profile />,
            // headerRight: <Icon style={{ marginRight: 20 }} name="flag" color="#000" size={24} />
        },
    },

    ProfileScreen: {
        screen: ProfileScreen,
        navigationOptions: () => ({
            headerTransparent: false
        })
    },

    PreferencesScreen: {
        screen: PreferencesScreen,
        navigationOptions: () => ({
            headerTransparent: false
        })
    },

    CreateProfileScreen: {
        screen: CreateProfileScreen,
        navigationOptions: () => ({
            headerTransparent: false
        })
    },

    UpdateProfileScreen: {
        screen: UpdateProfileScreen,
        navigationOptions: () => ({
            headerTransparent: false
        })
    },

    SellAnimalScreen: {
        screen: SellAnimalScreen,
        navigationOptions: () => ({
            headerTransparent: false
        })
    },

    SellProductScreen: {
        screen: SellProductScreen,
        navigationOptions: () => ({
            headerTransparent: false
        })
    },

    SellServiceScreen: {
        screen: SellServiceScreen,
        navigationOptions: () => ({
            headerTransparent: false
        })
    },

    SwiperScreen: {
        screen: SwiperScreen,
        navigationOptions: () => ({
            headerTransparent: true,
            headerTintColor: '#FFF'
        })
    },

    ConversationScreen: {
        screen: ConversationScreen,
        navigationOptions: () => ({
            headerTransparent: false
        })
    }
})

export default createAppContainer(AppNavigator)
