import React from 'react'
import { createAppContainer } from 'react-navigation'
import { createBottomTabNavigator } from 'react-navigation-tabs'
import Icon from 'react-native-vector-icons/FontAwesome'

import ArticleScreen from '../screens/App/Article/ArticleScreen'
import MatchScreen from '../screens/App/Match/MatchScreen'
import ChatScreen from '../screens/App/Chat/ChatScreen'
import MarketScreen from '../screens/App/Market/MarketScreen'

const HomeNavigator = createBottomTabNavigator(
    {
        Article: {
            screen: ArticleScreen,
            navigationOptions: {
                tabBarIcon: ({ focused, tintColor }) => (
                    focused ? <Icon name="globe" color={tintColor} size={28} /> : <Icon name="globe" color={tintColor} size={24} />
                )
            }
        },
        Market: {
            screen: MarketScreen,
            navigationOptions: {
                tabBarIcon: ({ focused, tintColor }) => (
                    focused ? <Icon name="cart-plus" color={tintColor} size={28} /> : <Icon name="cart-plus" color={tintColor} size={24} />
                )
            }
        },
        Match: {
            screen: MatchScreen,
            navigationOptions: {
                tabBarIcon: ({ focused, tintColor }) => (
                    focused ? <Icon name="paw" color={tintColor} size={28} /> : <Icon name="paw" color={tintColor} size={24} />
                )
            }
        },
        Chat: {
            screen: ChatScreen,
            navigationOptions: {
                tabBarIcon: ({ focused, tintColor }) => (
                    focused ? <Icon name="comments" color={tintColor} size={28} /> : <Icon name="comments" color={tintColor} size={24} />
                )
            }
        }
    },
    {
        initialRouteName: "Article",
        tabBarOptions: {
            showLabel: false,
            activeTintColor: '#FE7058',
            inactiveTintColor: '#444444'
        }
    }
)

export default createAppContainer(HomeNavigator)
