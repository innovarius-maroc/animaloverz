import { createAppContainer, createSwitchNavigator } from 'react-navigation'

import LoadingScreen from '../screens/LoadingScreen'
import AuthNavigator from './AuthNavigator'
import AppNavigator from './AppNavigator'

const StackNavigator = createSwitchNavigator({
    LoadingScreen: {
        screen: LoadingScreen,
        navigationOptions: {
            header: null
        }
    },

    AuthNavigator: {
        screen: AuthNavigator,
        navigationOptions: {
            header: null
        }
    },

    AppNavigator: {
        screen: AppNavigator,
        navigationOptions: {
            header: null
        }
    }
})

export default createAppContainer(StackNavigator)
