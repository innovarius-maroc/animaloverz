import { createAppContainer } from 'react-navigation'
import { createStackNavigator } from 'react-navigation-stack'

import LoginScreen from '../screens/Auth/LoginScreen'
import RegisterScreen from '../screens/Auth/RegisterScreen'

const AuthNavigator = createStackNavigator({
    LoginScreen: {
        screen: LoginScreen,
        navigationOptions: () => ({
            header: null,
            tabBarVisible: false,
            headerMode: "screen"
        })
    },

    RegisterScreen: {
        screen: RegisterScreen,
        navigationOptions: () => ({
            header: null,
            tabBarVisible: false,
            headerMode: "screen",
        })
    }
})

export default createAppContainer(AuthNavigator)
