import React, { useState, useEffect, useContext } from 'react'
import { Button, TextInput } from 'react-native-paper'
import { View, Alert, ImageBackground } from 'react-native'
import { translate, setI18nConfig } from '../../i18n/i18n'
import * as RNLocalize from "react-native-localize"
import Modal from 'react-native-modal'
import { AuthContext } from '../../../App'
import { loginWithEmail, resetPassword, createHumanProfile } from '../../context/AuthActions'
import { GoogleSignin, GoogleSigninButton } from 'react-native-google-signin'
import firebase from 'react-native-firebase'

const LoginScreen = props => {

    const { auth, authDispatch } = useContext(AuthContext)

    const [email, setEmail] = useState('')
    const [password, setPassword] = useState('')
    const [resetEmail, setResetEmail] = useState('')
    const [resetEmailModal, setResetEmailModal] = useState(false)

    // translation
    setI18nConfig()
    useEffect(() => {
        RNLocalize.addEventListener('change', setI18nConfig)
        return () => RNLocalize.removeEventListener('change', setI18nConfig)
    }, [])

    useEffect(() => {
        if (auth.error) {
            Alert.alert('', auth.error)
            auth.error = ''
        }
    }, [auth.error])

    const handleLogin = () => {
        if (!email || !password) {
            return alert(translate('pleaseFillInAllFields'))
        }

        if (!(/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(email))) {
            return alert(translate('pleaseEnterAValidEmailAddress'))
        }

        authDispatch({ type: 'LOGIN_ATTEMPT' })
        loginWithEmail(email.toLowerCase().trim(), password)
            .then(() => {
                authDispatch({ type: 'LOGIN_SUCCESS' })
                props.navigation.navigate('LoadingScreen')
            })
            .catch(error => authDispatch({ type: 'LOGIN_FAILED', error: error.message }))
    }

    const handlePasswordReset = () => {
        if (!resetEmail) {
            return alert(translate('pleaseFillInAllFields'))
        }

        if (!(/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(resetEmail))) {
            return alert(translate('pleaseEnterAValidEmailAddress'))
        }

        resetPassword(resetEmail).then(() => alert(translate('aPasswordResetLinkHasBeenSentToYourEmail')))
    }

    const googleLogin = async () => {
        try {
            authDispatch({ type: 'LOGIN_ATTEMPT' })
            GoogleSignin.configure({
                webClientId: '590703787257-r7d4fv1p82vmcdv4vfddrsfdm74f5475.apps.googleusercontent.com'
            })

            const data = await GoogleSignin.signIn()
            // return console.log(data)

            const credential = firebase.auth.GoogleAuthProvider.credential(data.idToken, data.accessToken)
            const firebaseUserCredential = await firebase.auth().signInWithCredential(credential)

            const userData = firebaseUserCredential.user

            firebase.firestore().collection('profiles').where('user', '==', userData.uid).get().then(profileSnap => {
                if (profileSnap.empty) {
                    createHumanProfile(userData.uid, userData.displayName, null, null).then(() => {
                        firebase.firestore().collection('profiles').add(humanProfile).then(() => {
                            authDispatch({ type: 'LOGIN_SUCCESS' })
                            props.navigation.navigate('LoadingScreen')
                        })
                    })
                } else {
                    props.navigation.navigate('LoadingScreen')
                }
            })
        } catch (error) {
            authDispatch({ type: 'LOGIN_FAILED', error: error.message })
            console.log(error)
        }
    }

    return (
        <View>
            <Modal
                isVisible={resetEmailModal}
                useNativeDriver={true}
                onBackButtonPress={() => setResetEmailModal(false)}
                onBackdropPress={() => setResetEmailModal(false)}>
                <View style={{ backgroundColor: '#FFF', padding: 10, borderRadius: 2 }}>
                    <TextInput
                        placeholder={translate('email')}
                        value={resetEmail}
                        style={{ backgroundColor: 'transparent' }}
                        onChangeText={resetEmail => setResetEmail(resetEmail)} />

                    <Button
                        uppercase={false}
                        mode="text"
                        style={{ backgroundColor: '#05A8AA', borderRadius: 2, marginTop: 10 }}
                        onPress={handlePasswordReset} >
                        {translate('resetPassword')}
                    </Button>
                </View>
            </Modal>

            <ImageBackground source={require('../../assets/images/background.png')} style={{ width: '100%', height: '100%' }} >
                <View style={{ width: '80%', height: '100%', alignSelf: 'center', justifyContent: 'center' }}>
                    <View style={{ borderRadius: 10, paddingHorizontal: 30, paddingVertical: 10, elevation: 8, backgroundColor: '#FFFFFF' }}>
                        <View>
                            <TextInput
                                placeholder={translate('email')}
                                value={email}
                                style={{ backgroundColor: 'transparent' }}
                                onChangeText={email => setEmail(email)} />

                            <TextInput
                                secureTextEntry={true}
                                placeholder={translate('password')}
                                value={password}
                                style={{ backgroundColor: 'transparent' }}
                                onChangeText={password => setPassword(password)} />
                        </View>

                        {/* {auth.error ? <Text style={{ color: 'red', alignSelf: 'center', marginTop: 10 }}>{auth.error}</Text> : null} */}

                        <View style={{ flexDirection: 'row' }}>
                            <Button
                                style={{ flex: 1, marginRight: 10, marginTop: 20, backgroundColor: '#05A8AA', borderRadius: 5, elevation: 4 }}
                                uppercase={false}
                                mode="text"
                                onPress={handleLogin}>
                                {translate('login')}
                            </Button>

                            <Button
                                style={{ flex: 1, marginLeft: 10, marginTop: 20, backgroundColor: '#FE7058', borderRadius: 5, elevation: 4 }}
                                uppercase={false}
                                mode="text"
                                onPress={() => props.navigation.navigate('RegisterScreen')}>
                                {translate('register')}
                            </Button>
                        </View>

                        <GoogleSigninButton
                            style={{ width: '100%', height: 48, marginTop: 10 }}
                            size={GoogleSigninButton.Size.Wide}
                            color={GoogleSigninButton.Color.Light}
                            onPress={googleLogin}
                            disabled={auth.loading} />

                        <Button
                            uppercase={false}
                            style={{ width: 160, alignSelf: 'center' }}
                            mode="text" color="#FE7058"
                            onPress={() => setResetEmailModal(true)}>
                            {translate('resetPassword')}
                        </Button>
                    </View>
                </View>
            </ImageBackground>
        </View>
    )
}

export default LoginScreen