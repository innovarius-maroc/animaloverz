import React, { useState, useEffect, useContext, useRef } from 'react'
import { View, Picker, Alert, ScrollView, ImageBackground, StyleSheet } from 'react-native'
import { TextInput, Button, ActivityIndicator } from 'react-native-paper'
import DatePicker from 'react-native-datepicker'

import { translate, setI18nConfig } from '../../i18n/i18n'
import * as RNLocalize from "react-native-localize"
import { AuthContext } from '../../../App'
import { register, logout } from '../../context/AuthActions'
import RNPickerSelect from 'react-native-picker-select'

const Register = props => {

    const { auth, authDispatch } = useContext(AuthContext)

    const datePickerRef = useRef(null)

    const [firstname, setFirstname] = useState('')
    const [lastname, setLastname] = useState('')
    const [gender, setGender] = useState('male')
    // const [date, setDate] = useState()
    const [email, setEmail] = useState('')
    const [password, setPassword] = useState('')
    const [passwordConf, setPasswordConf] = useState('')

    // translation
    setI18nConfig()
    useEffect(() => {
        RNLocalize.addEventListener('change', setI18nConfig)
        return () => RNLocalize.removeEventListener('change', setI18nConfig)
    }, [])

    // useEffect(() => {
    //     if (auth.error) {
    //         console.log(auth.error)
    //         auth.error = ''
    //     }
    // }, [auth.error])

    const handleRegister = () => {
        if (!firstname || !lastname || !gender /*|| !date */ || !email || !password || !passwordConf) {
            return alert(translate('pleaseFillInAllFields'))
        }

        const credentials = {
            name: firstname.toLowerCase().trim() + ' ' + lastname.toLowerCase().trim(),
            gender: null,
            birthday: null, //date,
            email: email.toLowerCase().trim(),
            password,
            passwordConf
        }

        authDispatch({ type: 'REGISTER_ATTEMPT' })
        register(credentials)
            .then(() => {
                authDispatch({ type: 'REGISTER_SUCCESS' })
                props.navigation.navigate('LoginScreen')
            })
            .catch(error => {
                alert(error)
                authDispatch({ type: 'REGISTER_FAILED', error })
            })
    }

    return (
        <View>
            <ImageBackground source={require('../../assets/images/background.png')} style={{ width: '100%', height: '100%' }} >
                <View style={{ width: '80%', height: '100%', alignSelf: 'center', justifyContent: 'center' }}>
                    <View style={{ width: '100%', borderRadius: 10, paddingHorizontal: 30, paddingVertical: 10, elevation: 8, backgroundColor: '#FFFFFF' }}>
                        <ScrollView style={{ width: '100%' }}>
                            <TextInput
                                placeholder={translate('firstName')}
                                value={firstname}
                                style={{ backgroundColor: 'transparent', marginBottom: 5 }}
                                onChangeText={firstname => setFirstname(firstname)} />

                            <TextInput
                                placeholder={translate('lastName')}
                                value={lastname}
                                style={{ backgroundColor: 'transparent', marginBottom: 5 }}
                                onChangeText={lastname => setLastname(lastname)} />

                            <View style={{ borderBottomColor: '#999999', borderBottomWidth: 1 }}>
                                <RNPickerSelect
                                    style={pickerSelectStyles}
                                    placeholder={{ label: 'select gender', value: null }}
                                    onValueChange={value => setGender(value)}
                                    items={[
                                        { label: translate('male'), value: 'male' },
                                        { label: translate('female'), value: 'female' },
                                        { label: translate('non-binary'), value: 'non-binary' }
                                    ]}
                                />
                            </View>

                            {/* <TextInput
                            placeholder={translate('dateOfBirth')}
                            value={date}
                            onFocus={() => datePickerRef.current.onPressDate()}
                            style={{ backgroundColor: 'transparent', marginBottom: 5 }} /> 
                            */}

                            <TextInput
                                placeholder={translate('email')}
                                value={email}
                                style={{ backgroundColor: 'transparent', marginBottom: 5 }}
                                onChangeText={email => setEmail(email)} />

                            <TextInput
                                secureTextEntry={true}
                                placeholder={translate('password')}
                                value={password}
                                style={{ backgroundColor: 'transparent', marginBottom: 5 }}
                                onChangeText={password => setPassword(password)} />

                            <TextInput
                                secureTextEntry={true}
                                placeholder={translate('confirmPassword')}
                                value={passwordConf}
                                style={{ backgroundColor: 'transparent', marginBottom: 5 }}
                                onChangeText={passwordConf => setPasswordConf(passwordConf)} />

                            {/* {auth.error ? <Text style={{ color: 'red', alignSelf: 'center', marginTop: 10 }}>{auth.error}</Text> : null} */}

                            <Button
                                style={{ marginTop: 20, backgroundColor: '#05A8AA', borderRadius: 5, elevation: 4 }}
                                uppercase={false}
                                mode="text"
                                onPress={handleRegister}>
                                {translate('register')}
                            </Button>
                            <Button
                                style={{ marginTop: 10, backgroundColor: '#FE7058', borderRadius: 5, elevation: 4 }}
                                uppercase={false}
                                mode="text"
                                onPress={() => props.navigation.goBack()}>
                                {translate('back')}
                            </Button>

                            <View style={{ marginBottom: 10 }}></View>

                            {/* <DatePicker
                            ref={datePickerRef}
                            date={date}
                            mode="date"
                            hideText
                            showIcon={false}
                            placeholder={translate('selectDate')}
                            format="YYYY-MM-DD"
                            minDate="1900-01-01"
                            maxDate={new Date()}
                            confirmBtnText="Confirm"
                            cancelBtnText="Cancel"
                            onDateChange={(date) => { setDate(date) }} />
                            */}

                        </ScrollView>
                    </View>
                </View>
            </ImageBackground>
        </View>
    )
}

const pickerSelectStyles = StyleSheet.create({
    inputIOS: {
        fontSize: 16,
        paddingVertical: 12,
        paddingHorizontal: 10,
        borderRadius: 5,
        color: 'black',
        paddingRight: 30, // to ensure the text is never behind the icon
    },
    inputAndroid: {
        fontSize: 16,
        paddingHorizontal: 10,
        paddingVertical: 8,
        borderRadius: 5,
        color: 'black',
        paddingRight: 30, // to ensure the text is never behind the icon
    },
})

export default Register