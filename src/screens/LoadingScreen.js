import React, { useState, useEffect, useContext } from 'react'
import { View, Image, Alert } from 'react-native'
import { Text } from 'react-native-paper'
import NetInfo from '@react-native-community/netinfo'
import { translate, setI18nConfig } from '../i18n/i18n'
import * as RNLocalize from "react-native-localize"
import { AuthContext, GeoContext } from '../../App'
import firebase, { firestore, messaging, notifications } from 'react-native-firebase'
import { getUserData, getUserProfiles } from '../context/AuthActions'
import { getGeolocation } from '../context/GeoActions'
import { getConversation } from '../helpers/chatHelper'

const LoadingScreen = props => {

    const { auth, authDispatch } = useContext(AuthContext)
    const { geo, geoDispatch } = useContext(GeoContext)

    // const [intent, setIntent] = useState({ type: '', data: '' })
    const [loadingState, setLoadingState] = useState('')

    let isAuthTriggered = false

    // translation
    setI18nConfig()
    useEffect(() => {
        RNLocalize.addEventListener('change', setI18nConfig)
        return () => RNLocalize.removeEventListener('change', setI18nConfig)
    }, [])

    // useEffect(() => {
    //     if (!intent) return

    //     if (intent.type === 'message') {
    //         getConversation(intent.data).then(conversation => {
    //             props.navigation.navigate('ConversationScreen', { conversation: conversation })
    //         })
    //     } else if (intent.type === 'main') {
    //         props.navigation.navigate('AppNavigator')
    //     }
    // }, [intent])

    useEffect(() => {
        // check connection
        const unsubscribe = NetInfo.addEventListener(state => {
            setLoadingState('checkingConnectivity')
            if (!state.isConnected) {
                setLoadingState('pleaseCheckYourInternetConnection')
            } else {
                setLoadingState('authenticating')

                firebase.auth().onAuthStateChanged(user => {
                    if (isAuthTriggered) return console.log(`already triggered`)

                    isAuthTriggered = true
                    console.log(`onAuthStateChanged`)
                    authDispatch({ type: 'LOGIN_ATTEMPT' })
                    // If user not found
                    if (!user) {
                        console.log(`no user`)
                        authDispatch({ type: 'LOGOUT_SUCCESS' })
                        isAuthTriggered = false
                        return props.navigation.navigate('AuthNavigator')
                    }

                    // If user found
                    console.log(`user ok`)
                    user.reload().then(() => {
                        console.log(`user reload`)
                        user = firebase.auth().currentUser
                        console.log(firebase.auth().currentUser.emailVerified)
                        if (user.emailVerified) {
                            console.log(`email verified`)
                            getUserData(user.uid).then(data => {
                                user.profiles = data.profiles
                                user.activeProfile = data.activeProfile

                                authDispatch({ type: 'LOGIN_SUCCESS', user })

                                // Get token & geolocation
                                setLoadingState('gettingGeolocation')
                                getFcmToken(user)
                                getLocation(user).then(() => {
                                    isAuthTriggered = false
                                    props.navigation.navigate('AppNavigator')
                                })

                                // notifications().getInitialNotification().then(notif => {
                                //     // if (notif.notification.data.type === 'message') {
                                //     //     return setIntent({ type: 'message', data: notif.notification.data.conversationId })
                                //     // }
                                //     console.log('notification')

                                //     firestore().collection('conversations').doc(notif.notification.data.conversationId).get().then(snap => {
                                //         const conversation = snap.data()
                                //         conversation.uid = snap.id

                                //         return props.navigation.navigate('ConversationScreen', { conversation })
                                //     })
                                // })

                                //return setIntent({ type: 'main', data: '' })
                            }).catch(() => authDispatch({ type: 'LOGIN_FAILED' }))
                        } else {
                            console.log(`email unverified`)

                            Alert.alert(
                                '',
                                translate('pleaseVerifyYourEmail'),
                                [
                                    {
                                        text: translate('ok'), onPress: () => {
                                            return props.navigation.navigate('AuthNavigator')
                                        }
                                    }
                                ]
                            )
                        }
                    })
                })
            }
        })

        return () => unsubscribe()
    }, [])

    const getLocation = (user) => {
        console.log(`getLocation`)
        return new Promise((resolve, reject) => {
            geoDispatch({ type: 'GEOLOCATION_FETCHING' })
            getGeolocation().then(geolocation => {
                const currentGeolocation = geolocation[0]
                currentGeolocation.currency = currencyCountry[currentGeolocation.countryCode]
                gatherLocationData(currentGeolocation)
                const promises = []

                if (user) {
                    firestore().collection('profiles').where('user', '==', user.uid).get().then(profilesSnap => {
                        profilesSnap.forEach(profile => {
                            const currentProfile = profile.data()
                            currentProfile.uid = profile.id

                            promises.push(firestore().collection('profiles').doc(currentProfile.uid).update({
                                geolocation: {
                                    countryCode: currentGeolocation.countryCode,
                                    country: currentGeolocation.country,
                                    city: currentGeolocation.locality,
                                    coords: currentGeolocation.position
                                }
                            }))
                        })

                        Promise.all(promises).then(() => {
                            console.log('geolocation added')
                            geoDispatch({ type: 'GEOLOCATION_SUCCESS', geolocation: currentGeolocation })
                            resolve()
                        })
                    })
                }
            }).catch(() => getLocation(user).then(() => props.navigation.navigate('AppNavigator')))
        })
    }

    const getFcmToken = (user) => {
        console.log(`getFcmToken`)
        return new Promise((resolve, reject) => {
            const promises = []
            let intent = {}

            promises.push(messaging().getToken().then(fcmToken => {
                if (fcmToken) {
                    const promises = []

                    getUserProfiles(user.uid).then(profiles => {
                        profiles.forEach(profile => {
                            promises.push(firestore().collection('profiles').doc(profile.uid).update({ fcmToken: fcmToken }))
                        })

                        Promise.all(promises).then(console.log('user token added'))
                    })
                }
                else {
                    console.log('user doesnt have a device token yet')
                }
            }))

            promises.push(messaging().onTokenRefresh(fcmToken => {
                const promises = []

                getUserProfiles(user.uid).then(profiles => {
                    profiles.forEach(profile => {
                        promises.push(firestore().collection('profiles').doc(profile.uid).update({ fcmToken: fcmToken }))
                    })

                    Promise.all(promises).then(console.log('user token updated'))
                })
            }))

            promises.push(notifications().onNotification((notification) => {
                // ToastAndroid.showWithGravity(notification.body, ToastAndroid.SHORT, ToastAndroid.CENTER)

                const notif = new firebase.notifications.Notification()
                    .setNotificationId(notification.notificationId)
                    .setTitle(notification.title)
                    .setBody(notification.body)

                notif.android.setChannelId(notification.notificationId)

                notifications().displayNotification(notif).catch(error => console.log(error))
            }))

            Promise.all(promises).then(() => resolve(intent))
        })
    }

    const gatherLocationData = (location) => {
        if (!location.locality) return

        firestore().collection('geolocationData').doc(location.countryCode).get().then(locationSnap => {
            if (locationSnap.exists) {
                const cities = locationSnap.data().cities
                if (!cities.includes(location.locality)) {
                    firestore().collection('geolocationData').doc(location.countryCode).update({
                        cities: [
                            ...locationSnap.data().cities,
                            location.locality
                        ]
                    })
                }
            }
            else {
                firestore().collection('geolocationData').doc(location.countryCode).set({
                    currency: currencyCountry[location.countryCode],
                    name: location.country,
                    cities: [location.locality]
                })
            }
        })
    }

    return (
        <View style={{ backgroundColor: '#DAE0E6', height: '100%', width: '100%' }}>
            <View style={{ justifyContent: 'center', alignItems: 'center', backgroundColor: 'transparent', width: '100%', height: '100%' }}>
                <Image style={{ width: 80, height: 80 }} source={require('../assets/images/logo.png')} />
                <Text style={{ marginTop: 10 }}>{translate(loadingState)}</Text>
            </View>
        </View>
    )
}

const currencyCountry = {
    "BD": "BDT",
    "BE": "EUR",
    "BF": "XOF",
    "BG": "BGN",
    "BA": "BAM",
    "BB": "BBD",
    "WF": "XPF",
    "BL": "EUR",
    "BM": "BMD",
    "BN": "BND",
    "BO": "BOB",
    "BH": "BHD",
    "BI": "BIF",
    "BJ": "XOF",
    "BT": "BTN",
    "JM": "JMD",
    "BV": "NOK",
    "BW": "BWP",
    "WS": "WST",
    "BQ": "USD",
    "BR": "BRL",
    "BS": "BSD",
    "JE": "GBP",
    "BY": "BYR",
    "BZ": "BZD",
    "RU": "RUB",
    "RW": "RWF",
    "RS": "RSD",
    "TL": "USD",
    "RE": "EUR",
    "TM": "TMT",
    "TJ": "TJS",
    "RO": "RON",
    "TK": "NZD",
    "GW": "XOF",
    "GU": "USD",
    "GT": "GTQ",
    "GS": "GBP",
    "GR": "EUR",
    "GQ": "XAF",
    "GP": "EUR",
    "JP": "JPY",
    "GY": "GYD",
    "GG": "GBP",
    "GF": "EUR",
    "GE": "GEL",
    "GD": "XCD",
    "GB": "GBP",
    "GA": "XAF",
    "SV": "USD",
    "GN": "GNF",
    "GM": "GMD",
    "GL": "DKK",
    "GI": "GIP",
    "GH": "GHS",
    "OM": "OMR",
    "TN": "TND",
    "JO": "JOD",
    "HR": "HRK",
    "HT": "HTG",
    "HU": "HUF",
    "HK": "HKD",
    "HN": "HNL",
    "HM": "AUD",
    "VE": "VEF",
    "PR": "USD",
    "PS": "ILS",
    "PW": "USD",
    "PT": "EUR",
    "SJ": "NOK",
    "PY": "PYG",
    "IQ": "IQD",
    "PA": "PAB",
    "PF": "XPF",
    "PG": "PGK",
    "PE": "PEN",
    "PK": "PKR",
    "PH": "PHP",
    "PN": "NZD",
    "PL": "PLN",
    "PM": "EUR",
    "ZM": "ZMK",
    "EH": "MAD",
    "EE": "EUR",
    "EG": "EGP",
    "ZA": "ZAR",
    "EC": "USD",
    "IT": "EUR",
    "VN": "VND",
    "SB": "SBD",
    "ET": "ETB",
    "SO": "SOS",
    "ZW": "ZWL",
    "SA": "SAR",
    "ES": "EUR",
    "ER": "ERN",
    "ME": "EUR",
    "MD": "MDL",
    "MG": "MGA",
    "MF": "EUR",
    "MA": "MAD",
    "MC": "EUR",
    "UZ": "UZS",
    "MM": "MMK",
    "ML": "XOF",
    "MO": "MOP",
    "MN": "MNT",
    "MH": "USD",
    "MK": "MKD",
    "MU": "MUR",
    "MT": "EUR",
    "MW": "MWK",
    "MV": "MVR",
    "MQ": "EUR",
    "MP": "USD",
    "MS": "XCD",
    "MR": "MRO",
    "IM": "GBP",
    "UG": "UGX",
    "TZ": "TZS",
    "MY": "MYR",
    "MX": "MXN",
    "IL": "ILS",
    "FR": "EUR",
    "IO": "USD",
    "SH": "SHP",
    "FI": "EUR",
    "FJ": "FJD",
    "FK": "FKP",
    "FM": "USD",
    "FO": "DKK",
    "NI": "NIO",
    "NL": "EUR",
    "NO": "NOK",
    "NA": "NAD",
    "VU": "VUV",
    "NC": "XPF",
    "NE": "XOF",
    "NF": "AUD",
    "NG": "NGN",
    "NZ": "NZD",
    "NP": "NPR",
    "NR": "AUD",
    "NU": "NZD",
    "CK": "NZD",
    "XK": "EUR",
    "CI": "XOF",
    "CH": "CHF",
    "CO": "COP",
    "CN": "CNY",
    "CM": "XAF",
    "CL": "CLP",
    "CC": "AUD",
    "CA": "CAD",
    "CG": "XAF",
    "CF": "XAF",
    "CD": "CDF",
    "CZ": "CZK",
    "CY": "EUR",
    "CX": "AUD",
    "CR": "CRC",
    "CW": "ANG",
    "CV": "CVE",
    "CU": "CUP",
    "SZ": "SZL",
    "SY": "SYP",
    "SX": "ANG",
    "KG": "KGS",
    "KE": "KES",
    "SS": "SSP",
    "SR": "SRD",
    "KI": "AUD",
    "KH": "KHR",
    "KN": "XCD",
    "KM": "KMF",
    "ST": "STD",
    "SK": "EUR",
    "KR": "KRW",
    "SI": "EUR",
    "KP": "KPW",
    "KW": "KWD",
    "SN": "XOF",
    "SM": "EUR",
    "SL": "SLL",
    "SC": "SCR",
    "KZ": "KZT",
    "KY": "KYD",
    "SG": "SGD",
    "SE": "SEK",
    "SD": "SDG",
    "DO": "DOP",
    "DM": "XCD",
    "DJ": "DJF",
    "DK": "DKK",
    "VG": "USD",
    "DE": "EUR",
    "YE": "YER",
    "DZ": "DZD",
    "US": "USD",
    "UY": "UYU",
    "YT": "EUR",
    "UM": "USD",
    "LB": "LBP",
    "LC": "XCD",
    "LA": "LAK",
    "TV": "AUD",
    "TW": "TWD",
    "TT": "TTD",
    "TR": "TRY",
    "LK": "LKR",
    "LI": "CHF",
    "LV": "EUR",
    "TO": "TOP",
    "LT": "LTL",
    "LU": "EUR",
    "LR": "LRD",
    "LS": "LSL",
    "TH": "THB",
    "TF": "EUR",
    "TG": "XOF",
    "TD": "XAF",
    "TC": "USD",
    "LY": "LYD",
    "VA": "EUR",
    "VC": "XCD",
    "AE": "AED",
    "AD": "EUR",
    "AG": "XCD",
    "AF": "AFN",
    "AI": "XCD",
    "VI": "USD",
    "IS": "ISK",
    "IR": "IRR",
    "AM": "AMD",
    "AL": "ALL",
    "AO": "AOA",
    "AQ": "",
    "AS": "USD",
    "AR": "ARS",
    "AU": "AUD",
    "AT": "EUR",
    "AW": "AWG",
    "IN": "INR",
    "AX": "EUR",
    "AZ": "AZN",
    "IE": "EUR",
    "ID": "IDR",
    "UA": "UAH",
    "QA": "QAR",
    "MZ": "MZN"
}

export default LoadingScreen
