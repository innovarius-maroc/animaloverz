import React, { useState, useEffect, useContext } from 'react'
import { ScrollView, View, Image, ActivityIndicator, TouchableOpacity, ToastAndroid, PermissionsAndroid } from 'react-native'
import { Text, TextInput, Button } from 'react-native-paper'
import { firestore, storage } from 'react-native-firebase'
import { AuthContext, GeoContext } from '../../../../App'
import { setI18nConfig, translate } from '../../../i18n/i18n'
import Icon from 'react-native-vector-icons/MaterialCommunityIcons'
import * as RNLocalize from 'react-native-localize'
import { Menu, MenuOptions, MenuOption, MenuTrigger, renderers } from 'react-native-popup-menu'
import ImagePicker from 'react-native-image-crop-picker'
import uuid from 'uuid/v4'

const SellProductScreen = props => {

    const { auth } = useContext(AuthContext)
    const { geo } = useContext(GeoContext)

    const [loading, setLoading] = useState(false)
    const [name, setName] = useState('')
    const [description, setDescription] = useState('')
    const [price, setPrice] = useState('')
    const [image0, setImage0] = useState('')
    const [image1, setImage1] = useState('')
    const [image2, setImage2] = useState('')
    const [location, setLocation] = useState(geo.geolocation)

    // translation
    setI18nConfig()
    useEffect(() => {
        RNLocalize.addEventListener('change', setI18nConfig)
        return () => RNLocalize.removeEventListener('change', setI18nConfig)
    }, [])

    const pickImage = (imageNumber) => {
        ImagePicker.openPicker({
            width: 500,
            height: 500,
            cropping: true
        }).then(image => {
            if (imageNumber === 0) setImage0({ path: image.path, filename: `IMG-${uuid()}` })
            if (imageNumber === 1) setImage1({ path: image.path, filename: `IMG-${uuid()}` })
            if (imageNumber === 2) setImage2({ path: image.path, filename: `IMG-${uuid()}` })
        })
    }

    const takeImage = (imageNumber) => {
        ImagePicker.openCamera({
            width: 500,
            height: 500,
            cropping: true
        }).then(image => {
            if (imageNumber === 0) setImage0({ path: image.path, filename: `IMG-${uuid()}` })
            if (imageNumber === 1) setImage1({ path: image.path, filename: `IMG-${uuid()}` })
            if (imageNumber === 2) setImage2({ path: image.path, filename: `IMG-${uuid()}` })
        })
    }

    const handleCreateOffer = () => {
        if (!name || !description || !price) {
            return alert(translate('pleaseFillInAllFields'))
        }

        if (!image0 || !image1 || !image2) {
            return alert(translate('pleasePickThreeImages'))
        }

        const offer = {
            type: 'product',
            author: auth.user.activeProfile.uid,
            name,
            description,
            price: price + ' ' + location.currency,
            images: [image0.filename, image1.filename, image2.filename],
            geolocation: {
                country: geo.geolocation.country,
                city: geo.geolocation.locality,
                countryCode: geo.geolocation.countryCode,
                coords: geo.geolocation.position
            },
            createdAt: new Date().toUTCString()
        }

        setLoading(true)
        const promises = []

        promises.push(storage().ref('images/' + image0.filename).putFile(decodeURI(image0.path)))
        promises.push(storage().ref('images/' + image1.filename).putFile(decodeURI(image1.path)))
        promises.push(storage().ref('images/' + image2.filename).putFile(decodeURI(image2.path)))

        Promise.all(promises).then(() => {
            console.log('ok')
            firestore().collection('offers').add(offer).then(() => {
                setLoading(false)
                props.navigation.goBack()
            })
        })
    }

    const handleCancelOffer = () => {
        props.navigation.goBack()
    }

    const renderImages = () => {
        let src0 = require('../../../assets/images/add-image.png')
        let src1 = require('../../../assets/images/add-image.png')
        let src2 = require('../../../assets/images/add-image.png')

        if (image0) src0 = { uri: image0.path }
        if (image1) src1 = { uri: image1.path }
        if (image2) src2 = { uri: image2.path }

        return (
            <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'center' }}>
                <Menu style={{ flex: 1 }} renderer={renderers.SlideInMenu}>
                    <MenuTrigger>
                        <Image style={{ width: 100, height: 100, alignSelf: 'center' }} source={src0} />
                    </MenuTrigger>
                    <MenuOptions>
                        <MenuOption onSelect={() => takeImage(0)} >
                            <View style={{ flexDirection: 'row', alignItems: 'center', paddingVertical: 8, paddingHorizontal: 10 }}>
                                <Icon style={{ marginRight: 15 }} name='camera' size={25} />
                                <Text>{translate('camera')}</Text>
                            </View>
                        </MenuOption>
                        <MenuOption onSelect={() => pickImage(0)} >
                            <View style={{ flexDirection: 'row', alignItems: 'center', paddingVertical: 8, paddingHorizontal: 10 }}>
                                <Icon style={{ marginRight: 15 }} name='image' size={25} />
                                <Text>{translate('image')}</Text>
                            </View>
                        </MenuOption>
                        {
                            image0 ?
                                <MenuOption onSelect={() => setImage0('')} >
                                    <View style={{ flexDirection: 'row', alignItems: 'center', paddingVertical: 8, paddingHorizontal: 10 }}>
                                        <Icon style={{ marginRight: 15 }} name='trash-can-outline' size={25} />
                                        <Text>{translate('remove')}</Text>
                                    </View>
                                </MenuOption>

                                : null
                        }
                    </MenuOptions>
                </Menu>

                <Menu style={{ flex: 1 }} renderer={renderers.SlideInMenu}>
                    <MenuTrigger>
                        <Image style={{ flex: 1, width: 100, height: 100, alignSelf: 'center' }} source={src1} />
                    </MenuTrigger>
                    <MenuOptions>
                        <MenuOption onSelect={() => takeImage(1)} >
                            <View style={{ flex: 1, flexDirection: 'row', alignItems: 'center', paddingVertical: 8, paddingHorizontal: 10 }}>
                                <Icon style={{ marginRight: 15 }} name='camera' size={25} />
                                <Text>{translate('camera')}</Text>
                            </View>
                        </MenuOption>
                        <MenuOption onSelect={() => pickImage(1)} >
                            <View style={{ flexDirection: 'row', alignItems: 'center', paddingVertical: 8, paddingHorizontal: 10 }}>
                                <Icon style={{ marginRight: 15 }} name='image' size={25} />
                                <Text>{translate('image')}</Text>
                            </View>
                        </MenuOption>
                        {
                            image1 ?
                                <MenuOption onSelect={() => setImage1('')} >
                                    <View style={{ flexDirection: 'row', alignItems: 'center', paddingVertical: 8, paddingHorizontal: 10 }}>
                                        <Icon style={{ marginRight: 15 }} name='trash-can-outline' size={25} />
                                        <Text>{translate('remove')}</Text>
                                    </View>
                                </MenuOption>

                                : null
                        }
                    </MenuOptions>
                </Menu>

                <Menu style={{ flex: 1 }} renderer={renderers.SlideInMenu}>
                    <MenuTrigger>
                        <Image style={{ width: 100, height: 100, alignSelf: 'center' }} source={src2} />
                    </MenuTrigger>
                    <MenuOptions>
                        <MenuOption onSelect={() => takeImage(2)} >
                            <View style={{ flexDirection: 'row', alignItems: 'center', paddingVertical: 8, paddingHorizontal: 10 }}>
                                <Icon style={{ marginRight: 15 }} name='camera' size={25} />
                                <Text>{translate('camera')}</Text>
                            </View>
                        </MenuOption>
                        <MenuOption onSelect={() => pickImage(2)} >
                            <View style={{ flexDirection: 'row', alignItems: 'center', paddingVertical: 8, paddingHorizontal: 10 }}>
                                <Icon style={{ marginRight: 15 }} name='image' size={25} />
                                <Text>{translate('image')}</Text>
                            </View>
                        </MenuOption>
                        {
                            image2 ?
                                <MenuOption onSelect={() => setImage2('')} >
                                    <View style={{ flexDirection: 'row', alignItems: 'center', paddingVertical: 8, paddingHorizontal: 10 }}>
                                        <Icon style={{ marginRight: 15 }} name='trash-can-outline' size={25} />
                                        <Text>{translate('remove')}</Text>
                                    </View>
                                </MenuOption>

                                : null
                        }
                    </MenuOptions>
                </Menu>
            </View>
        )
    }

    if (loading) return <ActivityIndicator size={60} />

    return (
        <View style={{ backgroundColor: '#EAEAEA', height: '100%', width: '100%' }}>
            <ScrollView style={{ backgroundColor: 'transparent', width: '100%', height: '100%' }}>
                <View style={{ margin: 20 }}>
                    <Text style={{ fontSize: 28, marginBottom: 10, textAlign: 'center' }}>{translate('createNewServiceOffer')}</Text>

                    {renderImages()}

                    <TextInput
                        underlineColorAndroid='transparent'
                        value={name}
                        style={{ backgroundColor: 'transparent' }}
                        placeholder={translate('name')}
                        onChangeText={text => setName(text)}
                    />

                    <TextInput
                        underlineColorAndroid='transparent'
                        value={description}
                        style={{ backgroundColor: 'transparent' }}
                        placeholder={translate('description')}
                        onChangeText={text => setDescription(text)}
                    />

                    <TextInput
                        underlineColorAndroid='transparent'
                        value={price}
                        style={{ backgroundColor: 'transparent' }}
                        keyboardType="numeric"
                        placeholder={translate('price')}
                        onChangeText={text => setPrice(text)}
                    />

                    <View style={{ marginTop: 10 }}>
                        <Button
                            style={{ backgroundColor: '#05A8AA', marginVertical: 5, borderRadius: 5, elevation: 4 }}
                            uppercase={false}
                            mode="text"
                            onPress={handleCreateOffer}>
                            {translate('createOffer')}
                        </Button>

                        <Button
                            style={{ backgroundColor: '#fe7058', marginVertical: 5, borderRadius: 5, elevation: 4 }}
                            uppercase={false}
                            mode="text"
                            onPress={handleCancelOffer}>
                            {translate('cancel')}
                        </Button>
                    </View>
                </View>
            </ScrollView>
        </View>
    )
}

export default SellProductScreen