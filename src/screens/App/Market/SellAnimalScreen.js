import React, { useState, useEffect, useContext } from 'react'
import { ScrollView, View, Image, ToastAndroid, ActivityIndicator } from 'react-native'
import { Text, TextInput, Button } from 'react-native-paper'
import { firestore } from 'react-native-firebase'
import { AuthContext, GeoContext } from '../../../../App'
import { setI18nConfig, translate } from '../../../i18n/i18n'
import * as RNLocalize from 'react-native-localize'

const SellAnimalScreen = props => {

    const { auth } = useContext(AuthContext)
    const { geo } = useContext(GeoContext)

    const [loading, setLoading] = useState(false)
    const [profile, setProfile] = useState(props.navigation.state.params.profile)
    const [description, setDescription] = useState('')
    const [price, setPrice] = useState('')
    const [location, setLocation] = useState(geo.geolocation)

    // translation
    setI18nConfig()
    useEffect(() => {
        RNLocalize.addEventListener('change', setI18nConfig)
        return () => RNLocalize.removeEventListener('change', setI18nConfig)
    }, [])

    const handleCreateOffer = () => {
        if (!description || !price) {
            return alert(translate('pleaseFillInAllFields'))
        }

        const offer = {
            type: 'animal',
            author: auth.user.activeProfile.uid,
            description,
            price: price + ' ' + location.currency,
            animal: profile.uid,
            geolocation: {
                country: geo.geolocation.country,
                city: geo.geolocation.locality,
                countryCode: geo.geolocation.countryCode,
                coords: geo.geolocation.position
            },
            createdAt: new Date().toUTCString()
        }

        setLoading(true)
        firestore().collection('offers').add(offer).then(() => {
            setLoading(false)
            props.navigation.goBack()
        })
    }

    const handleCancelOffer = () => {
        props.navigation.goBack()
    }

    if (loading) return <ActivityIndicator size={60} />

    return (
        <View style={{ backgroundColor: '#EAEAEA', height: '100%', width: '100%' }}>
            <ScrollView style={{ backgroundColor: 'transparent', width: '100%', height: '100%' }}>
                <View style={{ margin: 20 }}>
                    <Text style={{ fontSize: 28, marginBottom: 10, textAlign: 'center' }}>{translate('createNewAnimalOffer')}</Text>

                    {
                        profile.avatar ?

                            <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                                <Image source={{ uri: profile.avatar }} style={{ width: 100, height: 100, marginBottom: 10, borderRadius: 100 }} />
                            </View>

                            : null
                    }

                    <TextInput
                        underlineColorAndroid='transparent'
                        value={description}
                        style={{ backgroundColor: 'transparent' }}
                        placeholder={translate('description')}
                        onChangeText={text => setDescription(text)}
                    />

                    <TextInput
                        underlineColorAndroid='transparent'
                        value={price}
                        style={{ backgroundColor: 'transparent' }}
                        keyboardType="numeric"
                        placeholder={translate('price')}
                        onChangeText={text => setPrice(text)}
                    />

                    <View style={{ marginTop: 10 }}>
                        <Button
                            style={{ backgroundColor: '#05A8AA', marginVertical: 5, borderRadius: 5, elevation: 4 }}
                            uppercase={false}
                            mode="text"
                            onPress={handleCreateOffer}>
                            {translate('createOffer')}
                        </Button>
                        <Button
                            style={{ backgroundColor: '#fe7058', marginVertical: 5, borderRadius: 5, elevation: 4 }}
                            uppercase={false}
                            mode="text"
                            onPress={handleCancelOffer}>
                            {translate('cancel')}
                        </Button>
                    </View>
                </View>
            </ScrollView>
        </View>
    )
}

export default SellAnimalScreen
