import React, { useState, useEffect, useContext } from 'react'
import { ScrollView, View, TouchableOpacity, SafeAreaView, ToastAndroid, FlatList } from 'react-native'
import { Card, Text, Avatar } from 'react-native-paper'
import Modal from 'react-native-modal'
import Offer from '../../../Components/OfferComponent'
import { firestore } from 'react-native-firebase'
import Icon from 'react-native-vector-icons/MaterialCommunityIcons'
import { AuthContext, GeoContext } from '../../../../App'
import { getUserNonHumanProfiles, getProfileById, getImagesUrl } from '../../../context/AuthActions'
import moment from 'moment'
import { getDistance } from 'geolib'
import { setI18nConfig, translate } from '../../../i18n/i18n'
import * as RNLocalize from 'react-native-localize'
import ActionButton from 'react-native-action-button'
import Profiles from '../../../Components/ProfilesComponent'

// pagination
let last = null
let limit = 12

const MarketScreen = props => {

    const { auth } = useContext(AuthContext)
    const { geo } = useContext(GeoContext)

    const [loading, setLoading] = useState(false)
    const [profiles, setProfiles] = useState([])
    const [modalVisible, setModalVisible] = useState(false)
    const [offers, setOffers] = useState([])
    const [maxDistance, setMaxDistance] = useState(400)

    // translation
    setI18nConfig()
    useEffect(() => {
        RNLocalize.addEventListener('change', setI18nConfig)
        return () => RNLocalize.removeEventListener('change', setI18nConfig)
    }, [])

    // useEffect(() => {
    //     console.error(offers)
    // })

    useEffect(() => {
        if (!auth || !auth.user) return
        getUserNonHumanProfiles(auth.user.uid).then(profiles => {
            setProfiles(profiles)
        }).catch(error => console.error(error))
    }, [auth.user ? auth.user.profiles : null])

    useEffect(() => {
        if (auth && auth.user && auth.user.activeProfile) {
            handleRefresh()
        }
    }, [auth && auth.user ? auth.user.activeProfile : null])

    const handleFetchOffers = () => {
        setLoading(true)

        let query

        if (!last) {
            query = firestore().collection('offers')
                .orderBy('createdAt', 'DESC')
                .limit(limit)
        } else {
            query = firestore().collection('offers')
                .orderBy('createdAt', 'DESC')
                .startAfter(last)
                .limit(limit)
        }

        query.get().then(offersSnap => {
            if (offersSnap.empty) {
                return setLoading(false)
            }

            last = offersSnap.docs[offersSnap.docs.length - 1]
            const offersArray = []

            offersSnap.forEach(offer => {
                const currentOffer = offer.data()
                currentOffer.uid = offer.id
                getProfileById(currentOffer.author).then(author => {
                    currentOffer.author = author
                    if (currentOffer.type === 'animal') {
                        getProfileById(currentOffer.animal).then(animal => {
                            currentOffer.animal = animal
                            offersArray.push(currentOffer)
                            if (offersSnap.size === offersArray.length) {
                                offersArray.sort((a, b) => {
                                    if (moment(a.createdAt).valueOf() < moment(b.createdAt).valueOf()) return 1
                                    else if (moment(a.createdAt).valueOf() > moment(b.createdAt).valueOf()) return -1
                                    else return 0
                                })
                                const nearbyOffers = filterByDistance(offersArray)
                                if (offersArray.length) setOffers(offers => [...offers, ...nearbyOffers])
                                setLoading(false)
                            }
                        }).catch(error => console.error(error))
                    }
                    else if (currentOffer.type === 'product' || currentOffer.type === 'service') {
                        getImagesUrl(currentOffer.images).then(urls => {
                            currentOffer.images = urls
                            offersArray.push(currentOffer)
                            if (offersSnap.size === offersArray.length) {
                                setLoading(false)

                                offersArray.sort((a, b) => {
                                    if (moment(a.createdAt).valueOf() < moment(b.createdAt).valueOf()) return 1
                                    else if (moment(a.createdAt).valueOf() > moment(b.createdAt).valueOf()) return -1
                                    else return 0
                                })
                                const nearbyOffers = filterByDistance(offersArray)
                                if (offersArray.length) setOffers(offers => [...offers, ...nearbyOffers])
                                setLoading(false)
                            }
                        }).catch(error => console.error(error))
                    }
                    else {
                        const nearbyOffers = filterByDistance(offersArray)
                        if (offersArray.length) setOffers(offers => [...offers, ...nearbyOffers])
                        setLoading(false)
                    }
                }).catch(error => console.error(error))
            })
        }).catch(error => console.error(error))
    }

    const filterByDistance = (offers) => {
        const nearbyOffers = []

        offers.forEach(offer => {
            const distance = getDistance(
                { latitude: offer.geolocation.coords.lat, longitude: offer.geolocation.coords.lng },
                { latitude: geo.geolocation.position.lat, longitude: geo.geolocation.position.lng },
                1000
            )

            const distanceInKm = distance / 1000

            if (distanceInKm <= maxDistance) {
                offer.distance = distanceInKm
                nearbyOffers.push(offer)
            }
        })

        return nearbyOffers
    }

    const handleRefresh = () => {
        if (!loading) {
            setOffers([])
            last = null
            handleFetchOffers()
        }
    }

    const handleSellAnimal = () => {
        if (auth.user.activeProfile.type !== 'human') {
            return alert(translate('useYourHumanProfile'))
        }

        setModalVisible(true)
    }

    const handleSellProduct = () => {
        if (auth.user.activeProfile.type !== 'human') {
            return alert(translate('useYourHumanProfile'))
        }

        props.navigation.navigate('SellProductScreen')
    }

    const handleSellService = () => {
        if (auth.user.activeProfile.type !== 'human') {
            return alert(translate('useYourHumanProfile'))
        }

        props.navigation.navigate('SellServiceScreen')
    }

    const handleChooseAnimalToSell = (profile) => {
        setModalVisible(false)
        props.navigation.navigate('SellAnimalScreen', { profile })
    }

    const handleNewAnimalClick = () => {
        setModalVisible(false)
        props.navigation.navigate('CreateProfileScreen')
    }

    const renderProfiles = () => {
        return profiles.map(profile => {
            return (
                <TouchableOpacity key={profile.uid} onPress={() => handleChooseAnimalToSell(profile)}>
                    <View style={{ flex: 1, flexDirection: 'row', alignItems: 'center', marginVertical: 5 }}>
                        <Avatar.Image size={60} style={{ marginHorizontal: 10 }} source={{ uri: profile.avatar }} />
                        <Text style={{ flex: 2 }}>{profile.name}</Text>
                    </View>
                </TouchableOpacity>
            )
        })
    }

    const renderActionButton = () => (
        <ActionButton buttonColor='#FE7058' position='right' size={50} spacing={16} offsetY={50} >
            <ActionButton.Item buttonColor='#05A8AA' title={translate('animal')} onPress={handleSellAnimal}>
                <Icon name="paw" size={22} style={{ color: '#FFF' }} />
            </ActionButton.Item>
            <ActionButton.Item buttonColor='#05A8AA' title={translate('product')} onPress={handleSellProduct}>
                <Icon name="cart" size={22} style={{ color: '#FFF' }} />
            </ActionButton.Item>
            <ActionButton.Item buttonColor='#05A8AA' title={translate('service')} onPress={handleSellService}>
                <Icon name="dog-service" size={22} style={{ color: '#FFF' }} />
            </ActionButton.Item>
        </ActionButton>
    )

    if (!loading && offers.length === 0) return (
        <View style={{ backgroundColor: '#DAE0E6', height: '100%', width: '100%' }}>
            <Profiles />

            <View style={{ justifyContent: 'center', alignItems: 'center', backgroundColor: 'transparent', width: '100%', height: '100%' }}>
                <Text>{translate('empty')}</Text>
            </View>

            {renderActionButton()}
        </View>
    )

    return (
        <SafeAreaView style={{ backgroundColor: '#DAE0E6', height: '100%', width: '100%' }}>
            <Profiles />

            <Modal
                isVisible={modalVisible}
                useNativeDriver={true}
                onBackButtonPress={() => setModalVisible(false)}
                onBackdropPress={() => setModalVisible(false)}>
                <View style={{ backgroundColor: '#fff', justifyContent: 'center', alignItems: 'center', padding: 20 }}>
                    <ScrollView style={{ backgroundColor: 'transparent', width: '100%' }}>
                        <Text style={{ alignSelf: 'center', marginBottom: 10 }}>{translate('chooseAnAnimal')}</Text>

                        <TouchableOpacity onPress={handleNewAnimalClick}>
                            <View style={{ flex: 1, flexDirection: 'row', alignItems: 'center', marginVertical: 5 }}>
                                <Avatar.Image size={60} style={{ marginHorizontal: 10 }} source={require('../../../assets/images/plus.png')} />
                                <Text style={{ flex: 2 }}>{translate('createNewAnimalProfile')}</Text>
                            </View>
                        </TouchableOpacity>

                        {renderProfiles()}
                    </ScrollView>
                </View>
            </Modal>

            <FlatList
                data={offers}
                renderItem={({ item }) => <Offer offer={item} handleRefresh={handleRefresh} />}
                keyExtractor={offer => offer.uid}
                onEndReachedThreshold={0.2}
                onEndReached={handleFetchOffers}
                onRefresh={handleRefresh}
                refreshing={loading}
                ListFooterComponent={() => <View style={{ marginTop: 10 }}></View>}
            />

            {renderActionButton()}
        </SafeAreaView>
    )
}

export default MarketScreen
