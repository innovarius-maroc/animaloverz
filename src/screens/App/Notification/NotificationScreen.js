
import React from 'react'
import { ScrollView } from 'react-native'
import LinearGradient from 'react-native-linear-gradient'

class NotificationsScreen extends React.Component {
    render() {
        return (
            <LinearGradient start={{ x: 0.0, y: 0.1 }} end={{ x: 1.0, y: 1.0 }} colors={['#E7BD00', '#FE7058']}>
                <ScrollView style={{ backgroundColor: 'transparent', width: '100%', height: '100%' }}>

                </ScrollView>
            </LinearGradient>
        )
    }
}

export default NotificationsScreen