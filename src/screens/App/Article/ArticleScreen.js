import React, { useState, useEffect, useContext } from 'react'
import { ActivityIndicator, View, Picker, FlatList, SafeAreaView, ToastAndroid, StyleSheet } from 'react-native'
import { Text, Card, Button, TextInput } from 'react-native-paper'
import { setI18nConfig, translate } from '../../../i18n/i18n'
import * as RNLocalize from 'react-native-localize'
import { Image } from 'react-native-elements'
import Icon from 'react-native-vector-icons/MaterialCommunityIcons'
import Article from '../../../Components/ArticleComponent'
import LinkPreview from 'react-native-link-preview'
import firebase, { firestore } from 'react-native-firebase'
import Modal from 'react-native-modal'
import { AuthContext, GeoContext } from '../../../../App'
import { getProfileById } from '../../../helpers/profileHelper'
import moment from 'moment'
import { getDistance } from 'geolib'
import { ScrollView } from 'react-native-gesture-handler'
import ActionButton from 'react-native-action-button'
import ImagePicker from 'react-native-image-crop-picker'
import uuid from 'uuid/v4'
import RNPickerSelect from 'react-native-picker-select'
import Profiles from '../../../Components/ProfilesComponent'
//import AsyncStorage from '@react-native-community/async-storage'

// pagination
let last = null
let limit = 12

const ArticleScreen = props => {

    const { auth } = useContext(AuthContext)
    const { geo } = useContext(GeoContext)

    const [articles, setArticles] = useState([])
    const [fetchingArticles, setFetchingArticles] = useState(false)
    const [postingArticle, setPostingArticle] = useState(false)
    const [fetchingPreview, setFetchingPreview] = useState(false)
    const [postModalVisible, setPostModalVisible] = useState(false)
    const [imgSource, setImgSource] = useState('')
    const [maxDistance, setMaxDistance] = useState(400)
    const [article, setArticle] = useState({
        title: '',
        content: '',
        target: null,
        image: {
            path: '',
            filename: ''
        },
        link: {
            url: '',
            title: '',
            preview: ''
        },
        type: ''
    })

    // translation
    setI18nConfig()
    useEffect(() => {
        RNLocalize.addEventListener('change', setI18nConfig)
        return () => RNLocalize.removeEventListener('change', setI18nConfig)
    }, [])

    const pickImage = () => {
        ImagePicker.openPicker({
            width: 500,
            height: 500,
            cropping: true
        }).then(image => {
            setImgSource(image.path)
            setArticle({ ...article, type: 'image', image: { path: image.path, filename: `IMG-${uuid()}` } })
            setPostModalVisible(true)
        })
    }

    const takeImage = () => {
        ImagePicker.openCamera({
            width: 500,
            height: 500,
            cropping: true
        }).then(image => {
            setImgSource(image.path)
            setArticle({ ...article, type: 'image', image: { path: image.path, filename: `IMG-${uuid()}` } })
            setPostModalVisible(true)
        })
    }

    // fetch articles
    useEffect(() => {
        if (auth && auth.user && auth.user.activeProfile) handleRefresh()
    }, [auth && auth.user ? auth.user.activeProfile : null])

    const handleFetchArticles = (activeProfile) => {
        if (!activeProfile) return

        setFetchingArticles(true)

        let query

        if (!last) {
            query = firestore().collection('articles')
                .where('target', '==', activeProfile.type)
                .orderBy('createdAt', 'DESC')
                .limit(limit)
        } else {
            query = firestore().collection('articles')
                .where('target', '==', activeProfile.type)
                .orderBy('createdAt', 'DESC')
                .startAfter(last)
                .limit(limit)
        }

        query.get().then(articlesSnap => {
            if (articlesSnap.empty) return setFetchingArticles(false)

            last = articlesSnap.docs[articlesSnap.docs.length - 1]

            const articlesArray = []
            const promises = []

            articlesSnap.forEach(article => {
                const currentArticle = article.data()
                currentArticle.uid = article.id

                // fetch author
                promises.push(getProfileById(currentArticle.author).then(profile => {
                    currentArticle.author = profile
                    articlesArray.push(currentArticle)
                }))
            })

            Promise.all(promises).then(() => {
                // sort articles
                articlesArray.sort((a, b) => {
                    if (moment(a.createdAt).valueOf() < moment(b.createdAt).valueOf()) return 1
                    else if (moment(a.createdAt).valueOf() > moment(b.createdAt).valueOf()) return -1
                    else return 0
                })

                const nearbyArticles = filterByDistance(articlesArray)

                if (articlesArray.length) setArticles(articles => [...articles, ...nearbyArticles])
                setFetchingArticles(false)
            })
        })
    }

    const filterByDistance = (articles) => {
        const nearbyArticles = []

        articles.forEach(article => {
            // const distance = getDistance(
            //     { latitude: article.geolocation.coords.lat, longitude: article.geolocation.coords.lng },
            //     { latitude: auth.user.activeProfile.geolocation.coords.lat, longitude: auth.user.activeProfile.geolocation.coords.lng },
            //     1000
            // )

            const distance = getDistance(
                { latitude: article.geolocation.coords.lat, longitude: article.geolocation.coords.lng },
                { latitude: geo.geolocation.position.lat, longitude: geo.geolocation.position.lng },
                1000
            )

            const distanceInKm = distance / 1000

            if (distanceInKm <= maxDistance) {
                article.distance = distanceInKm
                nearbyArticles.push(article)
            }
        })

        return nearbyArticles
    }

    const handleRefresh = () => {
        if (!fetchingArticles) {
            setArticles([])
            last = null
            handleFetchArticles(auth.user.activeProfile)
        }
    }

    const postArticle = () => {
        // prevent empty article
        if (article.type === 'text') {
            if (!article.title || !article.content) {
                return alert(translate('pleaseFillInAllFields'))
            }
        }
        else if (article.type === 'link') {
            if (!article.title || !article.link.title) {
                return alert(translate('pleaseFillInAllFields'))
            }
        }
        else if (article.type === 'image') {
            if (!article.title || !article.image.path) {
                return alert(translate('pleaseFillInAllFields'))
            }
        }

        setPostingArticle(true)

        const newArticle = {
            author: auth.user.activeProfile.uid,
            type: article.type,
            target: article.target || auth.user.activeProfile.type,
            title: article.title,
            content: article.content,
            image: article.image.filename,
            link: article.link,
            geolocation: {
                country: geo.geolocation.country,
                city: geo.geolocation.locality,
                countryCode: geo.geolocation.countryCode,
                coords: geo.geolocation.position
            },
            likes: 0,
            shares: 0,
            createdAt: new Date().toUTCString()
        }

        if (article.type === 'image') {
            firebase.storage().ref('images/' + article.image.filename).putFile(decodeURI(article.image.path))
                .then(() => {
                    firebase.firestore().collection('articles').add(newArticle)
                        .then(() => {
                            setPostModalVisible(false)
                            setArticle({ ...article, title: '', content: '', image: {}, link: {}, type: '' })
                            setPostingArticle(false)
                            ToastAndroid.show('article posted', ToastAndroid.SHORT)
                            handleRefresh()
                        })
                })
        }
        else if (article.type === 'text' || article.type === 'link') {
            firebase.firestore().collection('articles').add(newArticle).then(() => {
                setPostModalVisible(false)
                setArticle({ ...article, title: '', content: '', image: {}, link: {}, type: '' })
                setPostingArticle(false)
                ToastAndroid.show('article posted', ToastAndroid.SHORT)
                handleRefresh()
            })
        }
    }

    const handleTextChange = (name, value) => {
        if (name === 'title') {
            setArticle({ ...article, title: value })
        }
        else if (name === 'content') {
            setArticle({ ...article, content: value })
        }
        else if (name === 'link') {
            setArticle({ ...article, link: { ...article.link, url: value } })

            setFetchingPreview(true)
            LinkPreview.getPreview(value)
                .then(data => {
                    setFetchingPreview(false)
                    setArticle({ ...article, link: { ...article.link, url: value, title: data.title, preview: data.images[0] } })
                })
        }
    }

    const handleArticleType = (articleType) => {
        setPostModalVisible(true)
        setArticle({ ...article, type: articleType })
    }

    const renderModal = () => {
        return (
            <Modal
                isVisible={postModalVisible}
                useNativeDriver={true}
                onBackButtonPress={() => setPostModalVisible(false)}
                onBackdropPress={handleCloseArticleModal}>
                <View style={{ justifyContent: 'center', alignItems: 'center' }}>

                    {
                        postingArticle ?

                            <ActivityIndicator size={60} />

                            :

                            <View style={{ width: '100%', backgroundColor: '#fff', padding: 20, borderRadius: 5 }}>
                                <ScrollView>

                                    {
                                        auth.user && auth.user.activeProfile.type === 'human' ?

                                            <View style={{ borderBottomColor: '#999999', borderBottomWidth: 1 }}>
                                                <RNPickerSelect
                                                    style={pickerSelectStyles}
                                                    placeholder={{ label: 'select target', value: null }}
                                                    onValueChange={value => setArticle({ ...article, target: value })}
                                                    items={[
                                                        { label: translate('humans'), value: 'human' },
                                                        { label: translate('dogs'), value: 'dog' },
                                                        { label: translate('cats'), value: 'cat' },
                                                        { label: translate('birds'), value: 'bird' },
                                                        { label: translate('horses'), value: 'horse' },
                                                    ]}
                                                />
                                            </View>

                                            : null
                                    }

                                    <TextInput
                                        underlineColorAndroid='transparent'
                                        value={article.title}
                                        style={{ marginBottom: 10, backgroundColor: 'transparent' }}
                                        onChangeText={value => handleTextChange('title', value)}
                                        placeholder={translate('title')}
                                    />

                                    {
                                        article.type === 'text' ?

                                            <TextInput
                                                multiline
                                                numberOfLines={4}
                                                underlineColorAndroid='transparent'
                                                value={article.content}
                                                style={{ backgroundColor: 'transparent' }}
                                                placeholder={translate('content')}
                                                onChangeText={value => handleTextChange('content', value)}
                                            />

                                            : null
                                    }

                                    {
                                        article.type === 'image' && imgSource ?

                                            <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                                                <Image source={{ uri: imgSource }} style={{ width: 200, height: 200, margin: 10, borderRadius: 5 }} />
                                            </View>

                                            : null
                                    }

                                    {
                                        article.type === 'link' ?

                                            <View>
                                                <TextInput
                                                    underlineColorAndroid='transparent'
                                                    value={article.link.url}
                                                    style={{ backgroundColor: 'transparent' }}
                                                    placeholder={translate('link')}
                                                    onChangeText={value => handleTextChange('link', value)}
                                                />

                                                {
                                                    fetchingPreview ?

                                                        <ActivityIndicator size={60} />

                                                        :

                                                        article.link.title ?

                                                            <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                                                                <Text style={{ marginTop: 10 }}>{article.link.title}</Text>
                                                                <Image source={{ uri: article.link.preview }} style={{ width: 250, height: 200, margin: 10, borderRadius: 5 }} />
                                                            </View>

                                                            : null
                                                }
                                            </View>

                                            : null
                                    }
                                    <View style={{ flexDirection: 'row', marginTop: 15 }}>
                                        <Button
                                            style={{ flex: 1, marginRight: 10, width: 100, backgroundColor: '#05A8AA', borderRadius: 5, elevation: 4 }}
                                            uppercase={false}
                                            mode="text"
                                            onPress={postArticle}>
                                            {translate('send')}
                                        </Button>
                                        <Button
                                            style={{ flex: 1, marginLeft: 10, width: 100, backgroundColor: '#FE7058', borderRadius: 5, elevation: 4 }}
                                            uppercase={false}
                                            mode="text"
                                            onPress={handleCloseArticleModal}>
                                            {translate('cancel')}
                                        </Button>
                                    </View>
                                </ScrollView>
                            </View>
                    }
                </View>
            </Modal>
        )
    }

    const handleCloseArticleModal = () => {
        setPostModalVisible(false)
        setArticle({ ...article, title: '', content: '', image: {}, link: {}, type: '' })
    }

    const renderActionButton = () => (
        <ActionButton buttonColor='#FE7058' position='right' size={50} spacing={16} offsetY={50} >
            <ActionButton.Item buttonColor='#05A8AA' title={translate('article')} onPress={() => handleArticleType('text')}>
                <Icon name="format-align-left" size={22} style={{ color: '#FFF' }} />
            </ActionButton.Item>
            <ActionButton.Item buttonColor='#05A8AA' title={translate('camera')} onPress={takeImage}>
                <Icon name="camera-outline" size={22} style={{ color: '#FFF' }} />
            </ActionButton.Item>
            <ActionButton.Item buttonColor='#05A8AA' title={translate('image')} onPress={pickImage}>
                <Icon name="image-outline" size={22} style={{ color: '#FFF' }} />
            </ActionButton.Item>
            <ActionButton.Item buttonColor='#05A8AA' title={translate('link')} onPress={() => handleArticleType('link')}>
                <Icon name="link" size={22} style={{ color: '#FFF' }} />
            </ActionButton.Item>
        </ActionButton>
    )

    if (!auth.user) return <ActivityIndicator />

    // if no articles exist
    if (!fetchingArticles && articles.length === 0) return (
        <SafeAreaView style={{ backgroundColor: '#DAE0E6', height: '100%', width: '100%' }}>
            <Profiles />

            <View style={{ justifyContent: 'center', alignItems: 'center', backgroundColor: 'transparent', width: '100%', height: '100%' }}>
                <Text>{translate('empty')}</Text>
            </View>

            {renderModal()}

            {renderActionButton()}
        </SafeAreaView>
    )

    return (
        <SafeAreaView style={{ backgroundColor: '#DAE0E6', height: '100%', width: '100%' }}>

            <Profiles />

                <FlatList
                    data={articles}
                    renderItem={({ item }) => <Article article={item} handleRefresh={handleRefresh} />}
                    keyExtractor={article => article.uid}
                    onEndReachedThreshold={0.2}
                    onEndReached={() => handleFetchArticles(auth.user.activeProfile)}
                    onRefresh={handleRefresh}
                    refreshing={fetchingArticles}
                    ListFooterComponent={() => <View style={{ marginTop: 10 }}></View>}
                />

                {renderModal()}

            {renderActionButton()}

        </SafeAreaView>
    )
}

const pickerSelectStyles = StyleSheet.create({
    inputIOS: {
        fontSize: 16,
        paddingVertical: 12,
        paddingHorizontal: 10,
        borderRadius: 5,
        color: 'black',
        paddingRight: 30, // to ensure the text is never behind the icon
    },
    inputAndroid: {
        fontSize: 16,
        paddingHorizontal: 10,
        paddingVertical: 8,
        borderRadius: 5,
        color: 'black',
        paddingRight: 30, // to ensure the text is never behind the icon
    },
})

export default ArticleScreen
