import React, { useState, useEffect, useContext } from 'react'
import { View, FlatList, SafeAreaView } from 'react-native'
import { Text } from 'react-native-paper'
import { translate, setI18nConfig } from '../../../i18n/i18n'
import * as RNLocalize from "react-native-localize"
import ChatItem from '../../../Components/ChatItemComponent'
import { AuthContext } from '../../../../App'
import { firestore } from 'react-native-firebase'
import { getProfileById } from '../../../helpers/profileHelper'
import { getLastMessageCreatedAt } from '../../../helpers/chatHelper'
import moment from 'moment'
import Profiles from '../../../Components/ProfilesComponent'

const ChatListScreen = props => {

    const { auth } = useContext(AuthContext)

    const [conversations, setConversations] = useState([])
    const [loading, setLoading] = useState(false)

    // translation
    setI18nConfig()
    useEffect(() => {
        RNLocalize.addEventListener("change", setI18nConfig)
        return () => RNLocalize.removeEventListener("change", setI18nConfig)
    }, [])

    useEffect(() => {
        if (auth && auth.user && auth.user.activeProfile) handleRefresh()
    }, [auth && auth.user ? auth.user.activeProfile : null])

    const handlefetchConversations = () => {
        setLoading(true)
        const fetchedConversations = []

        firestore().collection('conversations')
            .where('members', 'array-contains', auth.user.activeProfile.uid)
            .get().then(conversationsSnap => {
                if (conversationsSnap.empty) {
                    setLoading(false)
                    return setConversations([])
                }
                last = conversationsSnap.docs[conversationsSnap.docs.length - 1]

                const getMember = async (conv) => {
                    let member = {}

                    if (conv.members[0] !== auth.user.activeProfile.uid) {
                        member = await getProfileById(conv.members[0])
                    }
                    else {
                        member = await getProfileById(conv.members[1])
                    }

                    return member
                }

                conversationsSnap.forEach(conversation => {
                    const currentConversation = conversation.data()
                    currentConversation.uid = conversation.id

                    getMember(currentConversation).then(member => {
                        currentConversation.member = member

                        // getLastMessageCreatedAt(currentConversation.uid).then(lastMsgTime => {
                        // currentConversation.lastMsgTime = lastMsgTime
                        fetchedConversations.push(currentConversation)
                        if (fetchedConversations.length === conversationsSnap.size) {
                            // sort conversations
                            // fetchedConversations.sort((a, b) => {
                            //     if (moment(a.lastMsgTime).valueOf() < moment(b.lastMsgTime).valueOf()) return 1
                            //     else if (moment(a.lastMsgTime).valueOf() > moment(b.lastMsgTime).valueOf()) return -1
                            //     else return 0
                            // })

                            setLoading(false)
                            if (fetchedConversations.length) setConversations(fetchedConversations)
                        }
                        // })
                    })
                })
            })
    }

    const handleRefresh = () => {
        if (!loading) {
            setConversations([])
            handlefetchConversations()
        }
    }

    if (!loading && conversations.length === 0) return (
        <View style={{ backgroundColor: '#DAE0E6', height: '100%', width: '100%' }}>
            <View style={{ justifyContent: 'center', alignItems: 'center', backgroundColor: 'transparent', width: '100%', height: '100%' }}>
                <Text>{translate('empty')}</Text>
            </View>
        </View>
    )

    return (
        <SafeAreaView style={{ backgroundColor: '#DAE0E6', height: '100%', width: '100%' }}>

            <Profiles />

            <FlatList
                style={{ marginTop: 5 }}
                data={conversations}
                renderItem={({ item }) => <ChatItem conversation={item} />}
                keyExtractor={conversation => conversation.uid}
                // onEndReachedThreshold={0.2}
                // onEndReached={() => handlefetchConversations()}
                onRefresh={handleRefresh}
                refreshing={loading}
                ListFooterComponent={() => <View style={{ marginTop: 10 }}></View>}
            />

        </SafeAreaView>
    )
}

export default ChatListScreen
