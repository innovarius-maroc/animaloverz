import React, { useState, useEffect, useContext } from 'react'
import { GiftedChat, Bubble } from 'react-native-gifted-chat'
import { AuthContext } from '../../../../App'
import { firestore } from 'react-native-firebase'
import { fetchMessages, sendMessage } from '../../../helpers/chatHelper'
import { getProfileById } from '../../../helpers/profileHelper'
import MessageComponent from '../../../Components/MessageComponent'

const ChatListScreen = props => {

    const { auth } = useContext(AuthContext)

    const [conversation, setConversation] = useState(props.navigation.state.params.conversation)
    const [messages, setMessages] = useState([])

    // listen to messages in the conversation
    useEffect(() => {
        const unsubscribe = firestore().collection('conversations').doc(conversation.uid).onSnapshot(conversationSnap => {
            console.warn('listener')
            if (conversationSnap.data() && conversationSnap.data().messages && conversationSnap.data().messages.length === 0) return

            const messages = conversationSnap.data().messages
            const lastMessage = messages[messages.length - 1]

            let msg = {}

            if (lastMessage.author === auth.user.activeProfile.uid)
                msg = getAdaptedMessage(lastMessage, auth.user.activeProfile)
            else
                msg = getAdaptedMessage(lastMessage, conversation.member)

            if (messages.length > 0) {
                setMessages(previousMessages => GiftedChat.append(msg, previousMessages))
            }
        })

        fetchMessagesAdapted()

        return unsubscribe
    }, [])

    const fetchMessagesAdapted = () => {
        console.warn('fetchMessagesAdapted')
        fetchMessages(conversation.uid).then(messages => {
            if (messages.length === 0) {
                const msg = {
                    _id: 0,
                    text: 'Beginning of the conversation',
                    createdAt: new Date().toUTCString(),
                    system: true
                }

                return setMessages([msg])
            }

            const adaptedMessages = messages.map((currentMsg, index) => {
                let msg = {}

                if (currentMsg.author === auth.user.activeProfile.uid)
                    msg = getAdaptedMessage(currentMsg, auth.user.activeProfile)
                else
                    msg = getAdaptedMessage(currentMsg, conversation.member)

                return msg
            })

            setMessages(adaptedMessages)
        })
    }

    const handleOnSend = (messages = []) => {
        console.warn('send message')
        const msg = {
            _id: messages[0]._id,
            author: messages[0].user._id,
            text: messages[0].text,
            createdAt: messages[0].createdAt
        }

        sendMessage(auth.user.activeProfile.uid, conversation.member.uid, msg)
    }

    const getAdaptedMessage = (message, user) => {
        const msg = {
            _id: message._id,
            text: message.text,
            createdAt: new Date(message.createdAt.seconds * 1000),
            user: {
                _id: user.uid,
                name: user.name,
                avatar: user.avatar
            }
        }

        console.log(msg)
        return msg
    }

    return (
        <GiftedChat
            messages={messages}
            inverted={false}
            renderBubble={props => (
                <Bubble
                    {...props}
                    textStyle={{
                        right: {
                            color: '#FFF'
                        },
                        left: {

                        }
                    }}
                    wrapperStyle={{
                        right: {
                            backgroundColor: '#FE7D67',
                            borderRadius: 4,
                            minWidth: 70,
                        },
                        left: {
                            backgroundColor: '#EEEEEE',
                            borderRadius: 4,
                            minWidth: 70,
                        }
                    }}
                    containerToPreviousStyle={{
                        marginTop: 10
                    }}
                />
            )}
            onSend={messages => handleOnSend(messages)}
            onPressAvatar={user => getProfileById(user._id).then(profile => props.navigation.navigate('ProfileScreen', { profile }))}
            user={{
                _id: auth.user.activeProfile.uid,
                name: auth.user.activeProfile.name,
                avatar: auth.user.activeProfile.avatar
            }}
        />
    )
}

export default ChatListScreen
