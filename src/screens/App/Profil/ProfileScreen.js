import React, { useState, useEffect, useContext } from 'react'
import { View, ScrollView, Image, TouchableOpacity, Modal, TouchableWithoutFeedback, SafeAreaView } from 'react-native'
import { Card, Text } from 'react-native-paper'
import { withNavigation } from 'react-navigation'
import Article from '../../../Components/ArticleComponent'
import ImagePicker from 'react-native-image-picker'
import { firestore, storage } from 'react-native-firebase'
import Icon from 'react-native-vector-icons/FontAwesome'
import { setI18nConfig, translate } from '../../../i18n/i18n'
import * as RNLocalize from 'react-native-localize'
import { AuthContext, GeoContext } from '../../../../App'
import { createConversation, fetchConversation } from '../../../helpers/chatHelper'
import moment from 'moment'
import { getDistance } from 'geolib'

const ProfileScreen = props => {

    const { auth } = useContext(AuthContext)
    const { geo } = useContext(GeoContext)

    const [profile, setProfile] = useState(props.navigation.state.params.profile)
    const [articles, setArticles] = useState([])
    const [distance, setDistance] = useState(0)

    // translation
    setI18nConfig()
    useEffect(() => {
        RNLocalize.addEventListener('change', setI18nConfig)
        return () => RNLocalize.removeEventListener('change', setI18nConfig)
    }, [])

    useEffect(() => {
        const distance = getDistance(
            { latitude: profile.geolocation.coords.lat, longitude: profile.geolocation.coords.lng },
            { latitude: auth.user.activeProfile.geolocation.coords.lat, longitude: auth.user.activeProfile.geolocation.coords.lng },
            1000
        )

        setDistance(distance / 1000)
    }, [])

    useEffect(() => {
        const unsubscribe = firestore().collection('articles').where('author', '==', profile.uid).onSnapshot(articlesSnap => {
            const promises = []
            const articlesArray = []

            articlesSnap.forEach(article => {
                const currentArticle = article.data()
                currentArticle.uid = article.id
                // Fetch images
                if (currentArticle.type === 'image') {
                    promises.push(storage().ref('images/' + currentArticle.image).getDownloadURL()
                        .then(url => {
                            currentArticle.image = url
                        })
                    )
                }

                currentArticle.author = profile
                articlesArray.push(currentArticle)
            })

            Promise.all(promises).then(() => {
                articlesArray.sort((a, b) => {
                    if (moment(a.createdAt).valueOf() < moment(b.createdAt).valueOf()) return 1
                    else if (moment(a.createdAt).valueOf() > moment(b.createdAt).valueOf()) return -1
                    else return 0
                })

                const nearbyArticles = filterByDistance(articlesArray)

                setArticles(nearbyArticles)
            })
        })

        return unsubscribe
    }, [])

    const filterByDistance = (articles) => {
        const nearbyArticles = []

        articles.forEach(article => {
            const distance = getDistance(
                { latitude: article.geolocation.coords.lat, longitude: article.geolocation.coords.lng },
                { latitude: geo.geolocation.position.lat, longitude: geo.geolocation.position.lng },
                1000
            )
            const distanceInKm = distance / 1000

            article.distance = distanceInKm
            nearbyArticles.push(article)
        })

        return nearbyArticles
    }

    const handleAge = () => {
        let years = moment().diff(profile.birthDate, 'years')
        let months = moment().diff(profile.birthDate, 'months')

        if (profile.type !== 'human') {
            if (years === 0) return `${months} ${translate('months')}`
            else if (months === 0) return `${years} ${translate('years')}`
            else return `${years} ${translate('years')}, ${months - (years * 12)} ${translate('months')}`
        } else {
            return `${years} ${translate('years')}`
        }
    }

    const handleCreateConversation = () => {
        if (auth.user.activeProfile.type !== profile.type) {
            return alert(translate('youCanOnlyContactYourOwnType'))
        }

        createConversation([auth.user.activeProfile.uid, profile.uid]).then(() => {
            fetchConversation(auth.user.activeProfile.uid, profile.uid)
                .then(conversation => props.navigation.navigate('ConversationScreen', { conversation }))
        }).catch(error => alert(error))
    }

    const renderArticles = () => {
        if (articles) {
            return articles.map((article, key) => {
                if (article.type === 'text' || article.type === 'link') return (
                    <Article key={article.uid} article={article} />
                )
            })
        }
    }

    const renderImages = () => {
        if (articles) {
            const images = []

            articles.forEach(article => {
                if (article.type === 'image') images.push(article.image)
            })

            return articles.map((article, key) => {
                if (article.type === 'image') return (
                    <TouchableOpacity key={article.uid} onPress={() => props.navigation.navigate('SwiperScreen', { images, index: images.indexOf(article.image) })}>
                        <Image style={{ width: 100, height: 100, borderRadius: 2, margin: 5 }} source={{ uri: article.image }} />
                    </TouchableOpacity>
                )
            })
        }
    }

    const renderMessageIcon = () => {
        let isFound = false
        auth.user.profiles.forEach(currentProfile => {
            if (currentProfile.uid === profile.uid) {
                isFound = true
            }
        })

        if (isFound) return null

        return (
            <View style={{ borderBottomColor: '#ddd', borderBottomWidth: 1, paddingBottom: 5 }}>
                <TouchableOpacity onPress={handleCreateConversation}>
                    <Icon style={{ alignSelf: 'center' }} name='commenting-o' size={40} />
                </TouchableOpacity>
            </View>
        )
    }

    return (
        <SafeAreaView style={{ backgroundColor: '#EAEAEA', width: '100%', height: '100%' }}>
            <ScrollView style={{ backgroundColor: 'transparent', width: '100%', height: '100%' }}>

                <Card style={{ marginBottom: 5, elevation: 2 }}>
                    <View style={{ borderBottomColor: '#ddd', borderBottomWidth: 1, paddingBottom: 5 }}>
                        <Image source={require('../../../assets/images/background.png')} style={{ width: '100%', height: 200, resizeMode: 'cover' }} />
                        <TouchableWithoutFeedback onPress={() => props.navigation.navigate('SwiperScreen', { images: [profile.avatar], index: 0 })}>
                            <Image source={{ uri: profile.avatar }} style={{ borderColor: "#05A8AA", borderWidth: 2, width: 150, height: 150, borderRadius: 100, resizeMode: 'cover', position: 'absolute', top: 25, alignSelf: 'center' }} />
                        </TouchableWithoutFeedback>
                        <Text style={{ alignSelf: 'center', fontSize: 24 }}>{profile.name}</Text>
                    </View>

                    {renderMessageIcon()}

                    <View style={{ borderBottomColor: '#ddd', borderBottomWidth: 1, paddingVertical: 5 }}>
                        <View style={{ marginHorizontal: 10 }}>
                            {
                                profile.type === 'human' ?
                                    null : <Text>{handleAge()}</Text>
                            }
                            <Text>{translate(profile.type)}{profile.gender ? ', ' + translate(profile.gender) : null}</Text>
                            <Text>{distance} {translate('km')}</Text>
                        </View>
                    </View>

                    <View style={{ borderBottomColor: '#ddd', borderBottomWidth: 1, paddingVertical: 5 }}>
                        <View style={{ flexDirection: 'row' }}>
                            <ScrollView style={{ backgroundColor: 'transparent', width: '100%', height: '100%' }} horizontal={true} >
                                {renderImages()}
                            </ScrollView>
                        </View>
                    </View>
                </Card>

                {renderArticles()}

            </ScrollView>
        </SafeAreaView>
    )
}

export default withNavigation(ProfileScreen)
