import React, { useState, useContext, useEffect, useRef } from 'react'
import { View, Image, ScrollView, ActivityIndicator, StyleSheet, SafeAreaView } from 'react-native'
import { Text, TextInput, Button, Card } from 'react-native-paper'
import Icon from 'react-native-vector-icons/MaterialCommunityIcons'
import DatePicker from 'react-native-datepicker'
import { AuthContext, GeoContext } from '../../../../App'
import { createProfile } from '../../../context/AuthActions'
import { setI18nConfig, translate } from '../../../i18n/i18n'
import * as RNLocalize from 'react-native-localize'
import { Menu, MenuOptions, MenuOption, MenuTrigger, renderers } from 'react-native-popup-menu'
import ImagePicker from 'react-native-image-crop-picker'
import uuid from 'uuid/v4'
import RNPickerSelect from 'react-native-picker-select'

const CreateProfileScreen = props => {

    const { auth, authDispatch } = useContext(AuthContext)
    const { geo, geoDispatch } = useContext(GeoContext)

    const datePickerRef = useRef(null)

    const [name, setName] = useState('')
    const [gender, setGender] = useState('male')
    const [type, setType] = useState('dog')
    const [avatar, setAvatar] = useState({ path: '', filename: '', originalPath: '' })
    const [date, setDate] = useState('')
    const [loading, setLoading] = useState(false)

    // translation
    setI18nConfig()
    useEffect(() => {
        RNLocalize.addEventListener('change', setI18nConfig)
        return () => RNLocalize.removeEventListener('change', setI18nConfig)
    }, [])

    const pickImage = () => {
        ImagePicker.openPicker({
            width: 500,
            height: 500,
            cropping: true,
            cropperCircleOverlay: true
        }).then(image => setAvatar({ path: image.path, filename: `IMG-${uuid()}`, originalPath: image.path }))
    }

    const takeImage = () => {
        ImagePicker.openCamera({
            width: 500,
            height: 500,
            cropping: true,
            cropperCircleOverlay: true
        }).then(image => setAvatar({ path: image.path, filename: `IMG-${uuid()}`, originalPath: image.path }))
    }

    const cropImage = () => {
        ImagePicker.openCropper({
            path: avatar.originalPath,
            width: 500,
            height: 500,
            cropping: true,
            cropperCircleOverlay: true
        }).then(image => setAvatar({ ...avatar, path: image.path, filename: `IMG-${uuid()}` }))
    }

    const handleCreateProfile = () => {
        if (!name || !date || !gender || !type) {
            return alert(translate('pleaseFillInAllFields'))
        }

        if (!auth.user.uid) return alert('error')

        const newProfile = {
            name: name.toLowerCase(),
            gender: gender,
            type: type,
            birthDate: date,
            avatar: avatar,
            user: auth.user.uid,
            active: false,
            geolocation: {
                countryCode: geo.geolocation.countryCode,
                country: geo.geolocation.country,
                city: geo.geolocation.locality,
                coords: {
                    lat: geo.geolocation.position.lat,
                    lng: geo.geolocation.position.lng
                }
            },
            swipedRight: [],
            swipedLeft: []
        }

        setLoading(true)
        createProfile(newProfile, auth.user.uid).then(profiles => {
            setName('')
            setGender('')
            setType('')
            setAvatar({ path: '', filename: '', originalPath: '' })

            auth.user.profiles = profiles
            authDispatch({ type: 'PROFILE_CREATED', user: auth.user })
            setLoading(false)
            props.navigation.goBack()
        }).catch(error => {
            props.navigation.goBack()
            alert('error')
        })
    }

    const cancelProfile = () => {
        setName('')
        setGender('')
        setType('')
        setAvatar({ path: '', filename: '', originalPath: '' })

        props.navigation.goBack()
    }

    if (loading) return (
        <View style={{ justifyContent: 'center', alignItems: 'center', width: '100%', height: '100%' }}>
            <ActivityIndicator size={60} />
        </View>
    )

    return (
        <SafeAreaView style={{ backgroundColor: '#DAE0E6', height: '100%', width: '100%' }}>
            <ScrollView style={{ backgroundColor: 'transparent', width: '100%', height: '100%' }}>
                <View style={{ margin: 20 }}>

                    <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                        <Menu renderer={renderers.SlideInMenu}>
                            <MenuTrigger>
                                {
                                    avatar.path ?

                                        <Image source={{ uri: avatar.path }} style={{ width: 100, height: 100, marginBottom: 10, borderRadius: 100 }} />
                                        :
                                        <Image source={require('../../../assets/images/add-image.png')} style={{ width: 100, height: 100, marginBottom: 10, borderRadius: 100 }} />
                                }
                            </MenuTrigger>
                            <MenuOptions>
                                <MenuOption onSelect={takeImage}>
                                    <View style={{ flexDirection: 'row', alignItems: 'center', paddingVertical: 8, paddingHorizontal: 10 }}>
                                        <Icon style={{ marginRight: 15 }} name='camera' size={25} />
                                        <Text>camera</Text>
                                    </View>
                                </MenuOption>
                                <MenuOption onSelect={pickImage}>
                                    <View style={{ flexDirection: 'row', alignItems: 'center', paddingVertical: 8, paddingHorizontal: 10 }}>
                                        <Icon style={{ marginRight: 15 }} name='image' size={25} />
                                        <Text>pick image</Text>
                                    </View>
                                </MenuOption>
                                {
                                    avatar.path ?

                                        <View>
                                            <MenuOption onSelect={cropImage}>
                                                <View style={{ flexDirection: 'row', alignItems: 'center', paddingVertical: 8, paddingHorizontal: 10 }}>
                                                    <Icon style={{ marginRight: 15 }} name='crop' size={25} />
                                                    <Text>crop image</Text>
                                                </View>
                                            </MenuOption>
                                            <MenuOption onSelect={() => setAvatar({ filename: '', path: '', originalPath: '' })}>
                                                <View style={{ flexDirection: 'row', alignItems: 'center', paddingVertical: 8, paddingHorizontal: 10 }}>
                                                    <Icon style={{ marginRight: 15 }} name='delete' size={25} />
                                                    <Text>remove image</Text>
                                                </View>
                                            </MenuOption>
                                        </View>

                                        : null
                                }
                            </MenuOptions>
                        </Menu>
                    </View>

                    <TextInput
                        underlineColorAndroid='transparent'
                        value={name}
                        style={{ backgroundColor: 'transparent' }}
                        placeholder={translate('name')}
                        onChangeText={text => setName(text)}
                    />

                    <TextInput
                        placeholder={translate('dateOfBirth')}
                        value={date}
                        style={{ backgroundColor: 'transparent' }}
                        onFocus={() => datePickerRef.current.onPressDate()}
                        style={{ backgroundColor: 'transparent', marginBottom: 5 }} />

                    <View style={{ borderBottomColor: '#999999', borderBottomWidth: 1 }}>
                        <RNPickerSelect
                            style={pickerSelectStyles}
                            placeholder={{ label: 'select gender', value: null }}
                            onValueChange={value => setGender(value)}
                            items={[
                                { label: translate('male'), value: 'male' },
                                { label: translate('female'), value: 'female' }
                            ]}
                        />
                    </View>

                    <View style={{ borderBottomColor: '#999999', borderBottomWidth: 1 }}>
                        <RNPickerSelect
                            style={pickerSelectStyles}
                            placeholder={{ label: 'select animal type', value: null }}
                            onValueChange={value => setType(value)}
                            items={[
                                { label: translate('dog'), value: 'dog' },
                                { label: translate('cat'), value: 'cat' },
                                { label: translate('bird'), value: 'bird' },
                                { label: translate('horse'), value: 'horse' }
                            ]}
                        />
                    </View>

                    <View style={{ marginTop: 10 }}>
                        <Button
                            style={{ backgroundColor: '#05A8AA', marginVertical: 5, borderRadius: 5, elevation: 4 }}
                            uppercase={false}
                            mode="text"
                            onPress={handleCreateProfile}>
                            {translate('createProfile')}
                        </Button>
                        <Button
                            style={{ backgroundColor: '#fe7058', marginVertical: 5, borderRadius: 5, elevation: 4 }}
                            uppercase={false}
                            mode="text"
                            onPress={cancelProfile}>
                            {translate('cancel')}
                        </Button>
                    </View>
                </View>

                <DatePicker
                    ref={datePickerRef}
                    date={date}
                    style={{ position: 'absolute', top: -1000 }}
                    mode="date"
                    hideText
                    showIcon={false}
                    placeholder="select date"
                    format="YYYY-MM-DD"
                    minDate="1900-01-01"
                    maxDate={new Date()}
                    confirmBtnText="Confirm"
                    cancelBtnText="Cancel"
                    onDateChange={(date) => { setDate(date) }}
                />
            </ScrollView>
        </SafeAreaView>
    )
}

const pickerSelectStyles = StyleSheet.create({
    inputIOS: {
        fontSize: 16,
        paddingVertical: 12,
        paddingHorizontal: 10,
        borderRadius: 5,
        color: 'black',
        paddingRight: 30, // to ensure the text is never behind the icon
    },
    inputAndroid: {
        fontSize: 16,
        paddingHorizontal: 10,
        paddingVertical: 8,
        borderRadius: 5,
        color: 'black',
        paddingRight: 30, // to ensure the text is never behind the icon
    },
})

export default CreateProfileScreen
