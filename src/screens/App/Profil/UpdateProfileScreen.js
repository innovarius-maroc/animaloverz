import React, { useState, useEffect, useContext } from 'react'
import { View, Image, ScrollView, ActivityIndicator, SafeAreaView } from 'react-native'
import { Text, TextInput, Button } from 'react-native-paper'
import { AuthContext } from '../../../../App'
import { getUserData } from '../../../context/AuthActions'
import { firestore, storage } from 'react-native-firebase'
import { setI18nConfig, translate } from '../../../i18n/i18n'
import * as RNLocalize from 'react-native-localize'
import ImagePicker from 'react-native-image-crop-picker'
import uuid from 'uuid/v4'

const UpdateProfileScreen = props => {

    const { auth, authDispatch } = useContext(AuthContext)

    const [profile, setProfile] = useState(props.navigation.state.params.profile)
    const [name, setName] = useState('')
    const [avatar, setAvatar] = useState({ path: '', filename: '' })
    const [loading, setLoading] = useState(false)

    // translation
    setI18nConfig()
    useEffect(() => {
        RNLocalize.addEventListener('change', setI18nConfig)
        return () => RNLocalize.removeEventListener('change', setI18nConfig)
    }, [])

    useEffect(() => {
        setName(profile.name)
    }, [])

    const pickImage = () => {
        ImagePicker.openPicker({
            width: 500,
            height: 500,
            cropping: true,
            cropperCircleOverlay: true
        }).then(image => setAvatar({ path: image.path, filename: `IMG-${uuid()}` }))
    }

    const handleUpdateProfile = () => {
        if (!name) return alert(translate('pleaseFillInAllFields'))

        setLoading(true)
        if (avatar.filename) {
            storage().ref(`avatars/${avatar.filename}`).putFile(avatar.path).then(() => {
                firestore().collection('profiles').doc(profile.uid).get().then(profileSnap => {
                    const snapAvatar = profileSnap.data().avatar
                    if (snapAvatar !== 'human-avatar.png'
                        && snapAvatar !== 'dog-avatar.png'
                        && snapAvatar !== 'cat-avatar.png'
                        && snapAvatar !== 'horse-avatar.png'
                        && snapAvatar !== 'bird-avatar.png') {
                        storage().refFromURL(profile.avatar).delete()
                    }

                    firestore().collection('profiles').doc(profile.uid).update({
                        name: name.toLowerCase(),
                        avatar: avatar.filename
                    }).then(() => {
                        getUserData(auth.user.uid).then(user => {
                            authDispatch({ type: 'PROFILE_UPDATED', user: user })
                            setLoading(false)
                            props.navigation.goBack()
                        })
                    })
                }).catch(error => console.log(error))
            })
        }
        else {
            firestore().collection('profiles').doc(profile.uid).update({
                name: name.toLowerCase()
            }).then(() => {
                setLoading(false)
                props.navigation.goBack()
            })
        }
    }

    const cancelProfile = () => {
        setName('')
        setAvatar({ path: '', filename: '' })

        props.navigation.goBack()
    }

    if (loading) return (
        <View style={{ justifyContent: 'center', alignItems: 'center', width: '100%', height: '100%' }}>
            <ActivityIndicator size={60} />
        </View>
    )

    return (
        <SafeAreaView style={{ backgroundColor: '#EAEAEA', height: '100%', width: '100%' }}>
            <ScrollView style={{ backgroundColor: 'transparent', width: '100%', height: '100%' }}>
                <View style={{ margin: 20 }}>
                    <Text style={{ fontSize: 28, marginBottom: 10, textAlign: 'center' }}>Update profile</Text>

                    {
                        avatar.path ?

                            <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                                <Image source={{ uri: avatar.path }} style={{ width: 100, height: 100, marginBottom: 10, borderRadius: 100 }} />
                            </View>

                            :

                            <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                                <Image source={{ uri: profile.avatar }} style={{ width: 100, height: 100, marginBottom: 10, borderRadius: 100 }} />
                            </View>
                    }

                    <Button
                        style={{ backgroundColor: '#E7BD00', marginBottom: 10, borderRadius: 5, elevation: 4 }}
                        uppercase={false}
                        mode="text"
                        onPress={pickImage}>
                        {translate('selectImage')}
                    </Button>

                    <TextInput
                        value={name}
                        style={{ backgroundColor: 'transparent' }}
                        placeholder={translate('name')}
                        onChangeText={text => setName(text)}
                    />

                    <View style={{ marginTop: 10 }}>
                        <Button
                            style={{ backgroundColor: '#05A8AA', marginVertical: 5, borderRadius: 5, elevation: 4 }}
                            uppercase={false}
                            mode="text"
                            onPress={handleUpdateProfile}>
                            {translate('updateProfile')}
                        </Button>

                        <Button
                            style={{ backgroundColor: '#fe7058', marginVertical: 5, borderRadius: 5, elevation: 4 }}
                            uppercase={false}
                            mode="text"
                            onPress={cancelProfile}>
                            {translate('cancel')}
                        </Button>
                    </View>
                </View>
            </ScrollView>
        </SafeAreaView>
    )
}

export default UpdateProfileScreen
