import React, { useState, useEffect, useContext, useRef } from 'react'
import { View, TouchableOpacity, Image, ActivityIndicator, Platform, SafeAreaView } from 'react-native'
import { Text, Button } from 'react-native-paper'
import Icon from 'react-native-vector-icons/AntDesign'
import Swiper from 'react-native-deck-swiper'
import SwipeCard from '../../../Components/SwipeCardComponent'
import { AuthContext, GeoContext } from '../../../../App'
import { fetchProfilesToSwipe, swipeRight, swipeLeft } from '../../../helpers/swipeHelper'
import { fetchConversation } from '../../../helpers/chatHelper'
import { getDistance } from 'geolib'
import { setI18nConfig, translate } from '../../../i18n/i18n'
import * as RNLocalize from 'react-native-localize'
import Profiles from '../../../Components/ProfilesComponent'

var styleCard = require('../../../assets/css/card')

const MatchScreen = props => {

    const { auth } = useContext(AuthContext)
    const { geo } = useContext(GeoContext)

    const swiperRef = useRef(null)

    const [loading, setLoading] = useState(false)
    const [profiles, setProfiles] = useState([])
    const [maxDistance, setMaxDistance] = useState(400)

    // translation
    setI18nConfig()
    useEffect(() => {
        RNLocalize.addEventListener('change', setI18nConfig)
        return () => RNLocalize.removeEventListener('change', setI18nConfig)
    }, [])

    useEffect(() => {
        if (auth && auth.user && auth.user.activeProfile) getProfiles()
    }, [auth.user ? auth.user.activeProfile : null])

    const getProfiles = () => {
        if (!auth || !auth.user || !auth.user.activeProfile) return
        setLoading(true)
        fetchProfilesToSwipe(auth.user.activeProfile.uid, auth.user.activeProfile.type, auth.user.activeProfile.gender).then(profiles => {
            const nearbyProfiles = filterByDistance(profiles)
            // remove human profiles
            const nonHumanProfiles = []
            nearbyProfiles.forEach(profile => {
                if (profile.type !== 'human') {
                    nonHumanProfiles.push(profile)
                }
            })

            setProfiles(nonHumanProfiles)
            setLoading(false)
        })
    }

    const filterByDistance = (profiles) => {
        const nearbyProfiles = []

        profiles.forEach(profile => {
            const distance = getDistance(
                { latitude: profile.geolocation.coords.lat, longitude: profile.geolocation.coords.lng },
                { latitude: geo.geolocation.position.lat, longitude: geo.geolocation.position.lng },
                1000
            )

            const distanceInKm = distance / 1000

            if (distanceInKm <= maxDistance) {
                profile.distance = distanceInKm
                nearbyProfiles.push(profile)
            }
        })

        return nearbyProfiles
    }

    const handleSwiped = (direction, profileId) => {
        if (direction === 'right') {
            swipeRight(auth.user.activeProfile.uid, profileId).then(({ isMatch, swipedId }) => {
                if (isMatch) {
                    alert(translate('itsAMatch'))
                    fetchConversation(auth.user.activeProfile.uid, swipedId).then(conversation => {
                        props.navigation.navigate('ConversationScreen', { conversation })
                    })
                }
            })
        }
        else if (direction === 'left') {
            swipeLeft(auth.user.activeProfile.uid, profileId)
        }
    }

    if (auth.user.activeProfile.type === 'human') return (
        <View>
            <Profiles />

            <View style={{ backgroundColor: '#DAE0E6', alignItems: 'center', justifyContent: 'center', width: '100%', height: '100%' }}>
                <Text>{translate('usePetProfile')}</Text>
            </View>
        </View>
    )
    else {
        if (loading) return (
            <View>
                <Profiles />

                <View style={{ backgroundColor: '#DAE0E6', alignItems: 'center', justifyContent: 'center', width: '100%', height: '100%' }}>
                    <ActivityIndicator size={100} />
                </View>
            </View>
        )

        if (!loading && profiles.length === 0) return (
            <View>
                <Profiles />

                <View style={{ backgroundColor: '#DAE0E6', alignItems: 'center', justifyContent: 'center', width: '100%', height: '100%' }}>
                    <Text style={{ marginBottom: 20 }}>{translate('noProfilesAroundYouClickToRefresh')}</Text>
                    <TouchableOpacity onPress={getProfiles}>
                        <View style={styleCard.cardContentCenterIconsBorder}>
                            <Icon style={styleCard.Icon} name="reload1" color={'#05A8AA'} size={30} />
                        </View>
                    </TouchableOpacity>
                </View>
            </View>
        )

        return (
            <SafeAreaView style={{ backgroundColor: '#DAE0E6', height: '100%', width: '100%' }}>
                <Swiper
                    ref={swiperRef}
                    cards={profiles}
                    renderCard={(profile, index) => <SwipeCard key={profile ? profile.uid : index} activeProfile={auth.user.activeProfile} profile={profile} />}
                    onSwipedRight={index => handleSwiped('right', profiles[index].uid)}
                    onSwipedLeft={index => handleSwiped('left', profiles[index].uid)}
                    onSwipedAll={() => setProfiles([])}
                    animateCardOpacity
                    animateOverlayLabelsOpacity
                    disableBottomSwipe
                    disableTopSwipe
                    isAnimated
                    overlayLabels={overlayLabels}
                    useViewOverflow={Platform.OS === 'ios'}
                    backgroundColor={'#DAE0E6'}
                    cardVerticalMargin={70}
                    cardHorizontalMargin={10}
                    stackSize={3}>
                </Swiper>

                <View style={{ flexDirection: 'row', position: 'absolute', bottom: 10, alignSelf: 'center' }}>
                    <Button style={{ flex: 1, marginRight: 10, marginLeft: 20, padding: 5 }} uppercase icon="close" mode="contained" compact onPress={() => profiles.length > 0 ? swiperRef.current.swipeLeft() : null}>
                        nope
                    </Button>
                    <Button style={{ flex: 1, marginLeft: 10, marginRight: 20, padding: 5 }} uppercase icon="favorite" mode="contained" compact onPress={() => profiles.length > 0 ? swiperRef.current.swipeRight() : null}>
                        like
                    </Button>
                </View>

                <Profiles />
            </SafeAreaView>
        )
    }
}

const overlayLabels = {
    right: {
        element: <Image source={require('../../../assets/images/like.png')} style={{ height: 80, width: 80 }} />,
        style: {
            wrapper: {
                flexDirection: 'column',
                alignItems: 'flex-start',
                justifyContent: 'flex-start',
                marginTop: 20,
                marginLeft: 20
            }
        },
    },
    left: {
        element: <Image source={require('../../../assets/images/dislike.png')} style={{ height: 80, width: 80 }} />,
        style: {
            wrapper: {
                flexDirection: 'column',
                alignItems: 'flex-end',
                justifyContent: 'flex-start',
                marginTop: 20,
                marginLeft: -20
            }
        }
    }
}

export default MatchScreen
