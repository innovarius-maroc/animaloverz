import React, { useState, useEffect, useContext } from 'react'
import { ScrollView, Text, View } from 'react-native'
import Slider from 'react-native-slider'
import { AuthContext } from '../../../../App'
import { setI18nConfig, translate } from '../../../i18n/i18n'
import * as RNLocalize from 'react-native-localize'
import AsyncStorage from '@react-native-community/async-storage'

const PreferencesScreen = props => {

    const { auth, authDispatch } = useContext(AuthContext)

    const [maxDistance, setMaxDistance] = useState(20)

    // translation
    setI18nConfig()
    useEffect(() => {
        RNLocalize.addEventListener('change', setI18nConfig)
        return () => RNLocalize.removeEventListener('change', setI18nConfig)
    }, [])

    useEffect(() => {
        retrieveMaxDistance()
    }, [])

    const storeMaxDistance = async (distance) => {
        try {
            await AsyncStorage.setItem('maxDistance', distance.toString())
            console.log(`stored => ${distance}`)
        } catch (error) {
            console.warn(error)
        }
    }

    const retrieveMaxDistance = async () => {
        try {
            const distance = await AsyncStorage.getItem('maxDistance')
            if (distance !== null) {
                setMaxDistance(parseInt(distance))
            }
        } catch (error) {
            setMaxDistance(200)
            console.warn(error)
        }
    }

    return (
        <View>
            <ScrollView style={{ width: '100%', height: '100%' }}>
                <View>
                    <Text style={{ marginLeft: 10, marginTop: 20 }}>maximum distance: {maxDistance}</Text>
                    <Slider
                        style={{ marginHorizontal: 20, padding: 0 }}
                        value={maxDistance}
                        minimumValue={1}
                        maximumValue={200}
                        step={1}
                        onSlidingComplete={(distance) => storeMaxDistance(distance)}
                        thumbTintColor='#05A8AA'
                        minimumTrackTintColor='#05A8AA'
                        onValueChange={value => setMaxDistance(value)}
                    />
                </View>
            </ScrollView>
        </View>
    )
}

export default PreferencesScreen