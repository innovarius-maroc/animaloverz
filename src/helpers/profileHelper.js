import { firestore, storage } from 'react-native-firebase'

export const getAvatarUrl = (image) => {
    return new Promise((resolve, reject) => {
        storage().ref('avatars/' + image).getDownloadURL()
            .then(url => resolve(url))
            .catch(error => console.warn(error))
    })
}

export const getAllProfiles = () => {
    return new Promise((resolve, reject) => {
        firestore().collection('profiles').get().then(profilesSnap => {
            const profilesArray = []
            const promises = []

            profilesSnap.forEach(profile => {
                const currentProfile = profile.data()
                currentProfile.uid = profile.id
                
                promises.push(getAvatarUrl(currentProfile.avatar).then(url => {
                    currentProfile.avatar = url
                    profilesArray.push(currentProfile)
                }).catch(error => console.warn(error)))
            })
            
            Promise.all(promises).then(() => {
                console.log(profilesArray)
                resolve(profilesArray)
            }).catch(error => console.warn(error))
        }).catch(error => console.warn(error))
    })
}

export const getAllProfilesByTypeAndGender = (type, gender) => {
    return new Promise((resolve, reject) => {
        firestore().collection('profiles').where('type', '==', type).where('gender', '==', gender).get().then(profilesSnap => {
            const profilesArray = []
            const promises = []

            profilesSnap.forEach(profile => {
                const currentProfile = profile.data()
                currentProfile.uid = profile.id

                promises.push(getAvatarUrl(currentProfile.avatar).then(url => {
                    currentProfile.avatar = url
                    profilesArray.push(currentProfile)
                }).catch(error => console.warn(error)))
            })

            Promise.all(promises).then(() => {
                resolve(profilesArray)
            }).catch(error => console.warn(error))
        }).catch(error => console.warn(error))
    })
}

export const getUserIdFromProfile = (profileId) => {
    return new Promise((resolve, reject) => {
        firestore().collection('profiles').doc(profileId).get().then(profilesSnap => {
            const userId = profilesSnap.data().user
            resolve(userId)
        }).catch(error => console.warn(error))
    })
}

export const getUserProfiles = (userId) => {
    return new Promise((resolve, reject) => {
        firestore().collection('profiles').where('user', '==', userId).get().then(profilesSnap => {
            const profilesArray = []
            const promises = []

            profilesSnap.forEach(profile => {
                const currentProfile = profile.data()
                currentProfile.uid = profile.id

                // Fetch avatar
                promises.push(getAvatarUrl(currentProfile.avatar).then(url => {
                    currentProfile.avatar = url
                    profilesArray.push(currentProfile)
                }).catch(error => console.warn(error)))
            })

            Promise.all(promises).then(() => {
                resolve(profilesArray)
            }).catch(error => console.warn(error))
        }).catch(error => console.warn(error))
    })
}

export const getAllProfilesFromProfile = (profileId) => {
    return new Promise((resolve, reject) => {
        getUserIdFromProfile(profileId).then(userId => {
            getUserProfiles(userId).then(profiles => {
                resolve(profiles)
            }).catch(error => console.warn(error))
        }).catch(error => console.warn(error))
    })
}

export const getProfileById = (profileId) => {
    return new Promise((resolve, reject) => {
        firestore().collection('profiles').doc(profileId).get().then(profile => {
            const currentProfile = profile.data()
            currentProfile.uid = profile.id

            getAvatarUrl(currentProfile.avatar).then(url => {
                currentProfile.avatar = url
                resolve(currentProfile)
            }).catch(error => console.warn(error))
        }).catch(error => console.warn(error))
    })
}

export const getAllProfilesExcludingOwn = (profileId) => {
    return new Promise((resolve, reject) => {
        const profilesArray = []

        getUserIdFromProfile(profileId).then(userId => {
            getAllProfiles().then(allProfiles => {
                allProfiles.forEach(profile => {
                    if (profile.user !== userId) {
                        profilesArray.push(profile)
                    }
                })

                resolve(profilesArray)
            })
        }).catch(error => console.warn(error))
    })
}

export const removeProfile = (profileId) => {
    return new Promise((resolve, reject) => {
        // remove profile
        firestore().collection('profiles').doc(profileId).delete().then(() => {

            // remove the profile's articles
            firestore().collection('articles').where('author', '==', profileId).get().then(articlesSnap => {
                articlesSnap.forEach(article => {
                    // remove the article's images
                    if (article.type === 'image') {
                        storage().ref(`images/${article.image}`).delete()
                        article.ref.delete()
                    }
                    else {
                        article.ref.delete()
                    }
                })

                // remove the profile's conversations
                firestore().collection('conversations').where('members', 'array-contains', profileId).get().then(conversationsSnap => {
                    conversationsSnap.forEach(conversation => conversation.ref.delete())

                    resolve()
                }).catch(error => console.warn(error))
            }).catch(error => console.warn(error))
        }).catch(error => console.warn(error))
    })
}
