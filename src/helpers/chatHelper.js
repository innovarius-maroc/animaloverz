import { firestore } from 'react-native-firebase'
import { getProfileById, getAllProfilesFromProfile } from './profileHelper'

export const fetchConversations = (profileId) => {
    return new Promise((resolve, reject) => {
        const fetchedConversations = []

        firestore().collection('conversations').where('members', 'array-contains', profileId).get().then(conversationsSnap => {

            const getMember = async (conv) => {
                let member = {}

                if (conv.members[0] !== profileId) {
                    member = await getProfileById(conv.members[0])
                }
                else {
                    member = await getProfileById(conv.members[1])
                }

                return member
            }

            conversationsSnap.forEach(conversation => {
                const currentConversation = conversation.data()
                currentConversation.uid = conversation.id

                getMember(currentConversation).then(member => {
                    currentConversation.member = member
                    fetchedConversations.push(currentConversation)

                    if (fetchedConversations.length === conversationsSnap.size) {
                        resolve(fetchedConversations)
                    }
                }).catch(error => console.warn(error))
            })
        }).catch(error => console.warn(error))
    })
}

export const getLastMessageCreatedAt = (conversationId) => {
    return new Promise((resolve, reject) => {
        firestore().collection('conversations').doc(conversationId).get().then(conversationSnap => {
            const lastMessage = conversationSnap.data().messages[conversationSnap.data().messages.length - 1]
            resolve(new Date(lastMessage.createdAt.seconds * 1000))
        }).catch(error => console.warn(error))
    })
}

export const getConversation = (conversationId, profileId) => {
    return new Promise((resolve, reject) => {
        firestore().collection('conversations').doc(conversationId).get().then(conversationSnap => {
            const getMember = async (conv) => {
                let member = {}

                if (conv.members[0] !== profileId) {
                    member = await getProfileById(conv.members[0])
                }
                else {
                    member = await getProfileById(conv.members[1])
                }

                return member
            }

            const currentConversation = conversationSnap.data()
            currentConversation.uid = conversationSnap.id

            getMember(currentConversation).then(member => {
                currentConversation.member = member
                resolve(currentConversation)
            }).catch(error => console.warn(error))
        }).catch(error => console.warn(error))
    })
}

export const fetchConversation = (profileId, memberId) => {
    return new Promise((resolve, reject) => {
        firestore().collection('conversations').where('members', 'array-contains', profileId).get().then(conversationSnap => {
            conversationSnap.forEach(conversation => {
                const currentConversation = conversation.data()
                currentConversation.uid = conversation.id

                if (currentConversation.members.includes(memberId)) {
                    const indexOfMember = currentConversation.members.indexOf(memberId)
                    getProfileById(currentConversation.members[indexOfMember]).then(member => {
                        currentConversation.member = member
                        resolve(currentConversation)
                    }).catch(error => console.warn(error))
                }
            })
        }).catch(error => console.warn(error))
    })
}

export const fetchConversationById = (profileId, conversationId) => {
    return new Promise((resolve, reject) => {
        firestore().collection('conversations').doc(conversationId).get().then(conversationSnap => {
            if (conversationSnap.exists) {
                const currentConversation = conversationSnap.data()
                currentConversation.uid = conversationSnap.id

                const indexOfMember = currentConversation.members.indexOf(profileId) === 0 ? 1 : 0
                getProfileById(currentConversation.members[indexOfMember]).then(member => {
                    currentConversation.member = member
                    resolve(currentConversation)
                }).catch(error => console.warn(error))
            }
        }).catch(error => console.warn(error))
    })
}

export const fetchMessages = (conversationId) => {
    return new Promise((resolve, reject) => {
        firestore().collection('conversations').doc(conversationId).get().then(conversationSnap => {
            const messages = conversationSnap.data().messages
            resolve(messages)
        }).catch(error => console.warn(error))
    })
}

export const createConversation = ([activeProfileId, memberId]) => {
    return new Promise((resolve, reject) => {
        let isFound = false

        getAllProfilesFromProfile(activeProfileId).then(ownProfiles => {
            ownProfiles.forEach(profile => {
                if (profile.uid === memberId) {
                    isFound = true
                    reject('you cannot start a conversation with your own profiles')
                }
            })

            if (isFound) return
            isFound = false

            const conversation = {
                members: [activeProfileId, memberId],
                messages: []
            }

            firestore().collection('conversations').where('members', 'array-contains', activeProfileId).get().then(conversationsSnap => {
                if (conversationsSnap.empty) {
                    firestore().collection('conversations').add(conversation)
                    .then(ref => resolve(ref.id))
                    .catch(error => console.warn(error))
                    return
                }

                conversationsSnap.forEach(conversation => {
                    if (conversation.data().members.includes(memberId)) {
                        resolve(conversation.id)
                        isFound = true
                    }
                })

                if (!isFound) {
                    firestore().collection('conversations').add(conversation)
                    .then(ref => resolve(ref.id))
                    .catch(error => console.warn(error))
                }
            }).catch(error => console.warn(error))
        }).catch(error => console.warn(error))
    })
}

export const sendMessage = (senderId, receiverId, message) => {
    return new Promise((resolve, reject) => {
        firestore().collection('conversations').where('members', 'array-contains', senderId).get()
            .then(conversationsSnap => {
                conversationsSnap.forEach(currentConversation => {
                    if (currentConversation.data().members.includes(receiverId)) {
                        const conversation = currentConversation.data()
                        conversation.uid = currentConversation.id

                        currentConversation.ref.update({
                            messages: [
                                ...conversation.messages,
                                message
                            ]
                        }).then(() => resolve())
                        .catch(error => console.warn(error))
                    }
                })
            })
            .catch(error => console.warn(error))
    })
}
