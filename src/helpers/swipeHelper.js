import { firestore } from 'react-native-firebase'
import { getAllProfilesExcludingOwn } from './profileHelper'
import { createConversation } from './chatHelper'

export const fetchProfilesToSwipe = (activeProfileId, type, gender) => {
    return new Promise((resolve, reject) => {
        if (gender === 'male') gender = 'female'
        else if (gender === 'female') gender = 'male'

        const promises = []
        const filteredProfiles = []

        getAllProfilesExcludingOwn(activeProfileId).then(profiles => {
            profiles.forEach(profile => {
                if (profile.type === type && profile.gender === gender) {
                    promises.push(isSwipedRightOrLeftByProfile(activeProfileId, profile.uid).then(isSwiped => {
                        if (!isSwiped) {
                            filteredProfiles.push(profile)
                        }
                    }).catch(error => console.warn(error)))
                }
            })

            Promise.all(promises).then(() => {
                resolve(filteredProfiles)
            }).catch(error => console.warn(error))
        }).catch(error => console.warn(error))
    })
}

export const swipeRight = (activeProfileId, swipedProfileId) => {
    return new Promise((resolve, reject) => {
        firestore().collection('profiles').doc(activeProfileId).get().then(profile => {
            const swipedRightArray = profile.data().swipedRight

            if (!swipedRightArray.some(swipedRight => swipedRight.profile === swipedProfileId)) {
                firestore().collection('profiles').doc(activeProfileId).update({
                    swipedRight: [...swipedRightArray, { profile: swipedProfileId, swipedAt: new Date().toUTCString() }]
                }).then(() => {
                    isSwipedRightByProfile(swipedProfileId, activeProfileId).then(isSwipedRight => {
                        if (isSwipedRight) {
                            createConversation([activeProfileId, swipedProfileId]).then(() => {
                                resolve({ isMatch: true, swipedId: swipedProfileId})
                            }).catch(error => console.warn(error))
                        }
                        else {
                            resolve(false, null)
                        }
                    }).catch(error => console.warn(error))
                })
            }
        }).catch(error => console.warn(error))
    })
}

export const swipeLeft = (activeProfileId, swipedProfileId) => {
    return new Promise((resolve, reject) => {
        firestore().collection('profiles').doc(activeProfileId).get().then(profile => {
            const swipedLeftArray = profile.data().swipedLeft

            if (!swipedLeftArray.some(swipedLeft => swipedLeft.profile === swipedProfileId)) {
                firestore().collection('profiles').doc(activeProfileId).update({
                    swipedLeft: [...swipedLeftArray, { profile: swipedProfileId, swipedAt: new Date().toUTCString() }]
                }).then(() => resolve())
                .catch(error => console.warn(error))
            }
        }).catch(error => console.warn(error))
    })
}

export const isSwipedRightByProfile = (activeProfileId, swipedProfileId) => {
    return new Promise((resolve, reject) => {
        firestore().collection('profiles').doc(activeProfileId).get().then(profile => {
            const swipedRightArray = profile.data().swipedRight

            resolve(swipedRightArray.some(swipedRight => swipedRight.profile === swipedProfileId))
        }).catch(error => console.warn(error))
    })
}

export const isSwipedLeftByProfile = (activeProfileId, swipedProfileId) => {
    return new Promise((resolve, reject) => {
        firestore().collection('profiles').doc(activeProfileId).get().then(profile => {
            const swipedLeftArray = profile.data().swipedLeft

            resolve(swipedLeftArray.some(swipedLeft => swipedLeft.profile === swipedProfileId))
        }).catch(error => console.warn(error))
    })
}

export const isSwipedRightOrLeftByProfile = (activeProfileId, swipedProfileId) => {
    return new Promise((resolve, reject) => {
        firestore().collection('profiles').doc(activeProfileId).get().then(profile => {
            const swipedRightArray = profile.data().swipedRight
            const swipedLeftArray = profile.data().swipedLeft

            resolve(
                swipedRightArray.some(swipedRight => swipedRight.profile === swipedProfileId)
                || swipedLeftArray.some(swipedLeft => swipedLeft.profile === swipedProfileId)
            )
        }).catch(error => console.warn(error))
    })
}