import React, { createContext, useReducer } from "react"
import { DefaultTheme, Provider as PaperProvider } from 'react-native-paper'
import Navigation from "./src/Navigation/Navigation"
import { authReducer, authInitialState } from './src/context/AuthReducer'
import { GeoReducer, geoInitialState } from './src/context/GeoReducer'
import { MenuProvider } from 'react-native-popup-menu'
import IconFontAwesome from 'react-native-vector-icons/FontAwesome'
import IconMaterial from 'react-native-vector-icons/MaterialIcons'
import IconAnt from 'react-native-vector-icons/AntDesign'
import IconMaterialCommunity from 'react-native-vector-icons/MaterialCommunityIcons'

IconFontAwesome.loadFont()
IconMaterial.loadFont()
IconAnt.loadFont()
IconMaterialCommunity.loadFont()

const theme = {
    ...DefaultTheme,
    roundness: 0,
    colors: {
        ...DefaultTheme.colors,
        primary: '#FFFFFF',
        accent: '#FE7058'
    },
    // fonts: {
    //     regular: 'OpenSans-Regular',
    //     medium: 'OpenSans-Regular',
    //     light: 'OpenSans-Light',
    //     thin: 'OpenSans-Light'
    // }
}

export const AuthContext = createContext()
export const GeoContext = createContext()

const App = () => {

    const [authState, authDispatch] = useReducer(authReducer, authInitialState)
    const [geoState, geoDispatch] = useReducer(GeoReducer, geoInitialState)

    return (
        <AuthContext.Provider value={{ auth: authState, authDispatch }}>
            <GeoContext.Provider value={{ geo: geoState, geoDispatch }}>
                <PaperProvider theme={theme}>
                    <MenuProvider>
                        <Navigation />
                    </MenuProvider>
                </PaperProvider>
            </GeoContext.Provider>
        </AuthContext.Provider>
    )
}

export default App
