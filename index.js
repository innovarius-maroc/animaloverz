/**
 * @format
 */

import { AppRegistry } from 'react-native'
import App from './App'
import { name as appName } from './app.json'
import { YellowBox } from 'react-native'
import bgMessaging from './src/bgMessaging'

console.disableYellowBox = true

console.ignoredYellowBox = ['Warning: Each', 'Warning: Failed']
AppRegistry.registerComponent(appName, () => App)
AppRegistry.registerHeadlessTask('RNFirebaseBackgroundMessage', () => bgMessaging)

YellowBox.ignoreWarnings([
    'Warning: componentWillMount is deprecated',
    'Warning: componentWillReceiveProps is deprecated',
    'Module RCTImageLoader requires',
    'Setting a timer'
])
